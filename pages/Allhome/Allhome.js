// pages/Allhome/Allhome.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchKey: '', //搜索关键字
    totaltOrder: '', //今日订单统计
    // engineersInfo:'',
    // orderNumbers:[],
    width: 750,
    height: 200,
    speed: 0.1,
    step:0,
    top: app.globalData._network_path + 'flow1.png',
    mid: app.globalData._network_path + 'flow2.png',
    bot: app.globalData._network_path + 'flow3.png',
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var  that  =  this;
    app.loginStatus(function  ()  {
        // that.getEngMsg();
        that.setData({
          login_status:  true
        })
        // that.getOrderList();
        that.getTotaltOrder();//获取今日订单数
    },  function  ()  {
        that.setData({
          login_status:  false,
            engineersInfo:  ''
        });

    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  // 获取用户信息
  getEngMsg: function () {
    var that = this;
    app.ajax({
      url: "Engineer/Engineers/engineersInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            engineersInfo: res.data.data
          })
        } else {
          that.setData({
            engineersInfo: ''
          })
        }
      }
    })
  },
  // 跳转登录
  loginFun: function(e){
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/myCenter/login/login?type=' + type,
    })
  },
  // 跳转我的
  goMycenter: function(){
    wx.switchTab({
      url: '/pages/myCenter/home/home',
    })
  },
  // 跳转新订单
  goTakeorder: function(){
    wx.switchTab({
      url: '/pages/takeOrders/takeOrders/takeOrders',
    })
  },
  // 跳转未完成订单
  goOrder: function(){
    wx.switchTab({
      url: '/pages/order/myorder/myorder',
    })
  },
  // 跳转全部订单查看已完成
  goAllorder: function(){
    wx.navigateTo({
      url: '../order/myAllorder/myAllorder',
    })
  },
  // 跳转设备调试
  goDebug(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/test/test',
    })
  },
  // 跳转换芯管理
  goChangeOrder(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/order/changeOrder/changeOrder',
    })
  },
  // 跳转订单 0：待处理，1：已预约，2：处理中
  goOrderList:function(e){
    var that = this;
    var Type = e.currentTarget.dataset.type;
    console.log(Type);
    wx.setStorageSync("tabIndex", Type);
    wx.switchTab({
      url: '/pages/order/myorder/myorder',
    })
  },
  // 跳转日程管理
  goScheduleManage(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/date/scheduleManage/scheduleManage',
    })
  },
  // 跳转设备入库
  goWarehousing() {
    wx.navigateTo({
      url: '/pages/myCenter/warehousing/warehousing',
    })
  },
  // 跳转续费
  goRenewHome() {
    wx.navigateTo({
      url: '/pages/subpackage/pages/renew/renewHome/renewHome',
    })
  },
  // 设备管理
  goDeviceManage() {
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/deviceManage/deviceManage',
    })
  },
  // 输入搜索字段
  inputSearch: function (e) {
    var that = this;
    var searchKey = e.detail.value;
    that.setData({
      searchKey: searchKey
    });
  },
  //小键盘直接搜索
  searchOrder: function (e) {
    var that = this;
    var key = e.detail.value;
    if (key == '' || key == null) {
      app.showToast("请输入搜索关键字", "none", 2000, function () { });
      return;
    }
    that.setData({
      searchKey: key
    })
    wx.navigateTo({
      url: '/pages/order/myAllorder/myAllorder?keyword=' + key,
    })
  },
  //点击搜索图标搜索
  searchOrder2: function () {
    var that = this;
    var key = that.data.searchKey;
    if (key == '' || key == null) {
      app.showToast("请输入搜索关键字", "none", 2000, function () { });
      return;
    }
    wx.navigateTo({
      url: '/pages/order/myAllorder/myAllorder?keyword=' + key,
    })
  },
  //获取今日订单统计
  getTotaltOrder() {
    var that = this;
    app.ajax({
      url: "Engineer/Schedule/statistics",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            totaltOrder: res.data.data
          })
        }
      }
    })
  },
  //获取数量
  getOrderList: function () {
    var that = this;
    app.ajax({
      url: 'Engineer/Order/getRepairOrder',
      method: 'POST',
      load: that.data.load,
      msg: '数据加载中...',
      data: {
        work_order_status: 10,
      },
      success: function (res) {
        that.setData({
          orderNumbers: [res.data.data.work_num_8, res.data.data.work_num_9, res.data.data.work_num_10, res.data.data.work_num_11]
        });
      }
    })


  },
})