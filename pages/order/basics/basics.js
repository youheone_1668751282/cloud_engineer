// pages/order/basics/basics.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    search_type: 0,//搜索
    operation_type: 0, //是否编辑
    search_iocn: '../../../images/search_iocn.png',
    basicsPist: [],//配件数据
    checkedAll: false, //全选
    token: wx.getStorageSync('token'),//token
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
    work_order_id:'',//工单ID
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.work_order_id != '') {
      that.setData({
        work_order_id: options.work_order_id
      })
    }else{
      app.showToast("工单号为空请重试", "none", 2000, function () { });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    _PAGE = 1;
    that.getbasicsPist();

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getbasicsPist();
  },
  // 获取配件列表
  getbasicsPist: function (key) {
    var that = this;
    app.ajax({
      url: 'Common/Common/getBussinessType',
      load: load,
      msg: '基础服务加载中...',
      data: {
        work_order_id: that.data.work_order_id,
        page: _PAGE,
        pageSize: _PAGESIZE
      },
      header: {
        'token': that.data.token
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var basicsPist = res.data.data;
          for (var i in basicsPist) {
            basicsPist[i].checked = false;
            basicsPist[i].goods_num = 1;
          }
          //回显
          var basicsStor = wx.getStorageSync('basics') ? JSON.parse(wx.getStorageSync('basics')) : [];
          var checkedAll = false;
          if (_PAGE == 1) {
            if (basicsStor.length > 0) {
              for (var c in basicsStor) {
                for (var i in basicsPist) {
                  if (basicsStor[c].business_type_id == basicsPist[i].business_type_id) {
                    basicsPist[i].checked = true;
                    basicsPist[i].goods_num = basicsStor[c].goods_num;
                  }
                }
              }
            }
            if (basicsStor.length == basicsPist.length) {
              checkedAll = true;
            }
            that.setData({
              basicsPist: basicsPist,
              empty: false,
              checkedAll: checkedAll
            });
          } else {
            //分页的情况
            var getconcatList = that.data.basicsPist.concat(basicsPist);
            if (basicsStor.length == getconcatList.length) {
              checkedAll = true;
            }
            if (basicsStor.length > 0) {
              for (var c in basicsStor) {
                for (var i in getconcatList) {
                  if (basicsStor[c].business_type_id == getconcatList[i].business_type_id) {
                    getconcatList[i].checked = true;
                    getconcatList[i].goods_num = basicsStor[c].goods_num;
                  }
                }
              }
            }
            that.setData({
              basicsPist: getconcatList,
              empty: false
            });
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    })
  },
  //单选择
  checkboxChange: function (e) {
    let that = this;
    let index = e.currentTarget.dataset.index;
    let id = e.currentTarget.dataset.id;
    let checkboxItems = that.data.basicsPist;
    if (id ==3){
      app.showToast('新装不能勾选或者取消', "none", 3000, function () { });
      return false;
    }
    //   arr = e.detail.value;
    let len1 = checkboxItems.length,
      //   len2 = arr.length,
      len3 = 0;
    for (var i = 0; i < len1; i++) {
      //checkboxItems[i].checked = false;
      if (i == index) {
        checkboxItems[i].checked = !checkboxItems[i].checked;
      }
      if (checkboxItems[i].checked == true) {
        len3++;
      }
    }
    if (len3 == len1) {
      var checkedAll = true;
    } else {
      var checkedAll = false;
    }
    that.setData({
      basicsPist: checkboxItems,
      checkedAll: checkedAll
    })
  },
  //全选
  checkChange: function () {
    let that = this;
    let checkboxItems = that.data.basicsPist;
    let len = checkboxItems.length;
    let checkedAll = !that.data.checkedAll;
    var basicsPist = that.data.basicsPist;
    var is_ok = false;
    console.log('结果', basicsPist);
    for (var a in basicsPist) {
      if (basicsPist[a].business_type_id == 3 && basicsPist[a].checked==true) {
        console.log("进来");
        is_ok: true
      }
    }
    if (is_ok) {
      app.showToast('新装不能勾选或者取消', "none", 3000, function () { });
      return false;
    }
    for (var i = 0; i < len; i++) {
      checkboxItems[i].checked = checkedAll;
    }
    that.setData({
      checkedAll: checkedAll,
      basicsPist: checkboxItems
    })
  },
  //数量加减
  changeNUm: function (e) {
    var that = this;
    let type = e.currentTarget.dataset.type;
    let index = e.currentTarget.dataset.index;
    let num = that.data.basicsPist[index].goods_num;
    if (type == 0) {    //减
      num--;
    } else {          //加
      num++;
    }
    if (num < 1) {
      num = 1;
    }
    let goods_num = 'basicsPist[' + index + '].goods_num'
    that.setData({
      [goods_num]: num
    })
  },

  //跳转到选择配件
  newAdd: function () {
    var that = this;
    var basicsPist = that.data.basicsPist;
    var basicsList = [];
    var num = 0;
    for (var i in basicsPist) {
      if (basicsPist[i].checked == true) {
        basicsList[num] = basicsPist[i];
        num++;
      }
    }
    if (basicsList.length<=0){
      app.showToast('至少选择一项基础服务', "none", 3000, function () { });
      return false;
    }
    wx.setStorageSync('basics', JSON.stringify(basicsList));
    wx.navigateBack({
      delta: 1
    })
  }
})