// pages/order/dateSearch/dateSearch.js

var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderInfo: [],
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
    go_right: '../../../images/go_right.png',
    startdate: '',
    starttime: '',
    enddate: '',
    endtime: '',
    searchShow: false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getOrderList();
  },
  // 开始时间
  // 选择日期
  bindStartDateChange: function (e) {
    console.log(e.detail.value);
    var that = this;
    var startdate = e.detail.value;
    that.setData({
      startdate: startdate
    })
  },
  // 选择时间
  bindStartTimeChange: function (e) {
    console.log(e.detail.value);
    var that = this;
    var starttime = e.detail.value;
    that.setData({
      starttime: starttime
    })

  },
  // 截止时间
  // 选择日期
  bindEndDateChange: function (e) {
    console.log(e.detail.value);
    var that = this;
    var enddate = e.detail.value;
    that.setData({
      enddate: enddate
    })

  },
  // 选择时间
  bindEndTimeChange: function (e) {
    console.log(e.detail.value);
    var that = this;
    var endtime = e.detail.value;
    that.setData({
      endtime: endtime
    })

  },

  // 查询
  sureTime: function () {
    var that = this;
    var startdate = that.data.startdate;
    var starttime = that.data.starttime;
    var enddate = that.data.enddate;
    var endtime = that.data.endtime;
    if (startdate == '' || starttime == '' || enddate == '' || endtime == '') {
      app.showToast("请选择完整的日期与时间");
      return;
    }
    if (startdate > enddate){
      app.showToast("截止日期应大于等于开始日期");
      return;
    }
    if (startdate == enddate){
      
      if (starttime > endtime){
               
        app.showToast("截止时间应大于等于开始时间");
        return;
      }
    }
    _PAGE = 1;
    wx.showLoading({
      title: '加载中...',
    });
    that.setData({
      searchShow: true,
      orderInfo:[]
    })
    that.getOrderList();
  },

  // 获取付款记录列表
  getOrderList: function () {
    var that = this;
    var start_time = that.data.startdate + ' ' + that.data.starttime;
    var end_time = that.data.enddate + ' ' + that.data.endtime;
    app.ajax({
      url: 'Engineer/Engineers/getBill',
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
        start_time: start_time,
        end_time: end_time
      },
      success: function (res) {
        if (res.data.code == 1000) {
          wx.hideLoading();          
          if (_PAGE == 1) {
            that.setData({
              orderInfo: res.data.data,
              empty: false
            });
          } else {
            that.setData({
              orderInfo: that.data.orderInfo.concat(res.data.data),
              empty: false
            });
          }
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          // that.handleGroup(that.data.orderInfo);

        } else {
          wx.hideLoading();
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          load_show: true
        })
      }
    })

  },
  // 将数据处理分组
  handleGroup: function (arr) {
    var that = this;
    var copyArr = arr;
    var dateArr = [];
    for (var i in arr) {
      dateArr.push(arr[i].format_date);
    }
    var dateArr2 = dateArr.filter(function (element, index, self) {
      return self.indexOf(element) === index;
    });

    var group = [];
    for (var i in dateArr2) {
      var data = [];
      for (var j in arr) {
        if (arr[j].format_date == dateArr2[i]) {
          data.push(arr[j]);
        }
      }
      group.push({
        date: dateArr2[i],
        data: data
      })
    }
    that.setData({
      orderGroup: group
    })
    wx.hideLoading();
    console.log("group");
    console.log(group);

  },
})