// pages/order/receivables/receivables.js
var app = getApp();
var timeOver;
let savepath = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    work_order_id: '', //订单编号
    types: '',
    w_idth: 0, //手机宽度
    h_eight: 0, //手机高度
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if (options.work_order_id != '') {
      that.setData({
        work_order_id: options.work_order_id
      })
    } else {
      app.showToast("工单号为空请重试", "none", 2000, function() {});
    }
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          w_idth: res.windowWidth,
          h_eight: res.windowHeight,
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    that.getcode(); //获取支付验证码
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    clearInterval(timeOver); 
  },
  // 设置金额
  setMoney: function() {
    var that = this;
    wx.navigateTo({
      url: '../setMoney/setMoney',
    })

  },
  // 保存图片
  saveImg: function() {
    var that = this;
    var context = wx.createCanvasContext('myCanvas');
    let w_h = that.data.w_idth;
    let h_t = that.data.h_eight;
    var codeImg = '';//二维码图片
    var order_sn = that.data.order_sn;//订单编号
    var money = that.data.money;//金额
    var code_w = w_h*0.8*0.8;//二维码宽度
    wx.showLoading({
      title: '保存中...',
    })
    wx.getImageInfo({
      src: savepath,
      success: function(sres) {
        console.log('sres>>>>>',sres);
        console.log('order_sn>>>>>', order_sn);
        console.log('money>>>>>', money);

        codeImg = sres.path;
        context.fillStyle = '#fff';
        context.fillRect(0, 40, w_h*0.8, w_h);
        console.log('(w_h * 0.8 - w_h) / 2', (w_h * 0.8 - code_w) / 2)
        context.drawImage(codeImg, (w_h * 0.8 - code_w) / 2, 100, code_w, code_w);
        context.font = '20px';
        context.fillStyle = '#000';
        context.setTextAlign('center');
        context.fillText('订单编号：' + order_sn, (w_h * 0.8)/2,80);
        context.font = '30px';
        context.fillStyle = '#000';
        context.setTextAlign('center');
        context.fillText('￥' + money, (w_h * 0.8) / 2, 100 + code_w +30 );
        context.draw();
        setTimeout(function(){
          wx.canvasToTempFilePath({
            canvasId: 'myCanvas',
            fileType: 'jpg',
            success: function (res) {
              wx.saveImageToPhotosAlbum({
                filePath: res.tempFilePath,
                success(res) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '保存成功',
                  });
                },
                fail() {
                  wx.hideLoading()
                }
              })
            },
            fail(res) {
              console.log('失败了', res);
            }
          })
        },2000);
        
      }
    })
  },
  //支付方式
  getcode() {
    var that = this,
      work_order_id = that.data.work_order_id;
    app.ajax({
      url: 'Engineer/Pay/scanPay',
      data: {
        work_order_id: work_order_id
      },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            order_sn: res.data.data.order_sn,
            url: res.data.data.url,
            money: res.data.data.money
          })
          savepath = res.data.data.url;
          timeOver = setInterval(() => {
            that.getState(res.data.data.order_sn);
          }, 1000)
        } else {
          // 没有数据
        }
      }
    })
  },
  //
  getState(order_sn) {
    var that = this;
    app.ajax({
      url: 'Engineer/Pay/queryPayStatus',
      data: {
        work_order_number: order_sn
      },
      success: function(res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message + '2秒后跳转',"none",2000);
          clearInterval(timeOver);
          wx.setStorageSync('tabIndex', 3);
          wx.removeStorageSync('basics'); //清除缓存
          wx.removeStorageSync('filterelement'); //清除缓存
          wx.removeStorageSync('service'); //清除缓存
          wx.removeStorageSync('valcontent'); //清除缓存
          wx.removeStorageSync('showImg'); //清除缓存
          wx.removeStorageSync('upImg'); //清除缓存  
          wx.removeStorageSync('armature'); //清除缓存
          wx.removeStorageSync('need_charge');//清除缓存
          wx.removeStorageSync('changemoney');//清除缓存
          setTimeout(() => {
            wx.switchTab({
              url: '../myorder/myorder',
            })
          }, 1000);
        } else {

          // 没有数据
        }
      }
    })
  },
  // 返回
  returnBack(){
    var that = this;
    wx.showModal({
      title: '提示',
      content: '请确认用户是否未支付返回',
      success(res){
        if(res.confirm){
          wx.switchTab({
            url: '../myorder/myorder',
          })
        }
        if(res.cancel){

        }
      }
    })
  }
})