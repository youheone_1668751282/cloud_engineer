// pages/import/import.js
var util = require("../../../utils/util.js");
var app = getApp();
var moreup = false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    values: '',
    scanning: '../../../images/scanning.png',
    work_order_id: '',
    equipment_info:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      work_order_id: options.work_order_id
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    moreup = false;
    this.getOrderDetail();
  },
  // 订单详情
  getOrderDetail: function () {
    var that = this;
    var id = that.data.work_order_id;
    app.ajax({
      url: 'Engineer/Workorder/workOrderDetail',
      data: { work_order_id: id},
      success: function (res) {
        if(res.data.code==1000){
          console.log(res.data.data.equipment_info[0].device_no,"res.data.data.equipment_info[0].device_no")
          that.setData({
            equipment_info: res.data.data.equipment_info,
            values: res.data.data.equipment_info[0].device_no
          })
        }
        

      }
    })

  },
  //input 输入框
  inputfun(e) {
    var that = this;
    that.setData({
      values: e.detail.value
    })
  },
  // 扫码
  scanning: function () {
    var that = this;
    app.scanning(function (data) {
      that.setData({
        values: data
      })
    })
    // wx.scanCode({
    //   success: (res) => {

    //     console.log(res)
    //     var code = res.result.split(';')
    //     if (!util.isExitsVariable(code) || code.length == 0) {
    //       app.showToast('扫码失败');
    //       return false;
    //     }

    //     var val = code[2].split(':');
    //     if (!util.isExitsVariable(val) || val.length == 0) {
    //       app.showToast('扫码失败');
    //       return false;
    //     }

    //     that.setData({
    //       values: val[1]
    //     })

    //   },
    //   fail: (res) => {
    //     app.showToast('扫码失败，请重试！');
    //   }
    // })
  },
  //确认事件 设备信息
  affirmfun(e) {
    var that = this;
    if (that.data.values == '' || that.data.values == null) {
      app.showToast("请输入设备编号", "none", 2000, function () { });
      return;
    }
    if (moreup) {
      return false
    }
    moreup = true;
    var device_no = that.data.values;
    // 判断设备是否存在
    app.ajax({
      url: 'Engineer/Equipmentlists/detail',
      data: { "device_no": device_no },
      success: function (res) {

        if (res.data.code == 1000) {
          wx.navigateTo({
            url: '../activationDetail/activationDetail?device_no=' + device_no,
          })
        } else {
          app.showToast(res.data.message);
          moreup = false;
          return false;
        }
      }
    })


  },
  // 复制设备编号
  copyNumber(e){
    var that = this;
    var number = e.currentTarget.dataset.number;
    console.log('number',number);
    that.setData({
      values: number
    })
    wx.setClipboardData({
      data: number,
    })
  }
})