// pages/order/amendTime/amendTime.js
var app = getApp();
let time_id = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    go_right: '../../../images/go_right.png',
    work_order_id: '',//工单ID
    date: '',
    timeArr:[],//时间段数组
    time_index: '',//时间段index
    startTime:'',
    endTime: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.setData({
      work_order_id: id
    });
    time_id = '';

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.getRangeTime();
    that.getOrderDetail();
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 获取预约时间范围
  getRangeTime:function(){
    let that = this;
    app.ajax({
      url: 'Common/Common/getRangeTime',
      data:{},
      success:function(res){
        let timeArr = res.data.data.time;   
        var start = res.data.data.date.start;
        var end = res.data.data.date.end;
        that.setData({
          timeArr: timeArr,
          startTime: start,
          endTime: end
        })
        
      }
    })
  },
  // 获取订单详情
  getOrderDetail:function(){
    let that = this;
    app.ajax({
      url: 'Engineer/Workorder/workOrderDetail',
      data: { work_order_id: that.data.work_order_id},
      success: function(res){
        if(res.data.code == 1000){
          time_id = res.data.data.appointment_time_id;
          let date = res.data.data.appointment_time_date;
          that.setData({
            date: date,
          });
          let timeArr = that.data.timeArr;
          for (let i in timeArr) {
            if (timeArr[i].time_id == time_id){
              that.setData({
                time_index: i,
              })
              break;
            }
          }

        }
      }
    })

  },
  //选择日期
  bindDateChange: function (e) {
    let that = this;
    let date = e.detail.value;
    that.setData({
      date: date
    })
  },
  //选择时间段
  bindPickerChange: function (e) {
    let that = this;
    let time_index = e.detail.value;
    let timeArr = that.data.timeArr;
    that.setData({
      time_index: time_index,
    })
    time_id = timeArr[time_index].time_id;
  },
  // 确认预约时间
  sureTime: function(e){
    let that = this;
    let id = that.data.work_order_id;
    let date = that.data.date;
    let timeArr = that.data.timeArr;
    let time_index = that.data.time_index;
    //app.saveFormId(e.detail.formId);
    if (date != '' && time_id!=''){
      wx.showModal({
        title: '提示',
        content: '是否确认预约时间为：\r\n' + date + ',' + timeArr[time_index].format,
        success(res){
          if(res.confirm){
            app.ajax({
              url: 'Engineer/Workorder/editAppointmentTime',
              data: {
                work_order_id: id,
                appointment_date: date,
                appointment_time_id: time_id
              },
              success: function (res) {
                if (res.data.code == 1000) {
                  app.showToast("确认预约成功", "none", 2000, function () { });
                  wx.switchTab({
                    url: '../../order/myorder/myorder'
                  });
                  wx.setStorageSync('tabIndex', 1);

                } else {
                  app.showToast(res.data.message, "none", 2000, function () { });
                }
              }
            })
          }
          if(res.cancel){

          }
        }
      })
    }else{      
      app.showToast('请选择时间','none',2000,function(){})
    }
  }
})