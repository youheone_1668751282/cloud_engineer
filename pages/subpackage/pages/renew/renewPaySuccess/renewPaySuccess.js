// pages/user/renewPaySuccess/renewPaySuccess.js

var app = getApp();
var is_auth = 1;//是否认证
Page({


  /**
   * 页面的初始数据
   */
  data: {
    user_id: '',//用户i
    order_id: '',
    equipment_id: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      order_id: options.order_id,
      equipment_id: options.equipment_id
    })
    that.getUserInfo();
  },

  //获取用户信息
  getUserInfo: function () {
    var that = this;
    app.ajax({
      url: 'User/User/getUserInfo',
      method: "POST",
      data: {},
      success: function (res) {
        //console.log(res)
        if (res.data.code == 1000) {
          is_auth = res.data.data.is_auth;
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //返回首页
  returnBack: function () {
    var that = this;
    wx.switchTab({
      url: '/pages/Allhome/Allhome',
    })
  },
  //续费记录
  record(){
    wx.navigateTo({
      url: '../renewRecord/renewRecord',
    })
  }
})