// pages/Engineer/renew/renew.js
var app = getApp();
var util = require("../../../utils/util.js");
var bannerImg = app.globalData._network_path + 'renew_banner.jpg';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerImg: bannerImg,
    logoImg:'',
    equipment_id: '',  // 设备ID
    equipmentDetail:[],    // 设备详情
    contract_id: '',//合同id
    contractInfo: {},
    coupon:[],//返回的优惠券ID集合
    renew_money:0.00,
    coupon_number:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      equipment_id: options.equipment_id ? options.contract_id : '',
      contract_id: options.contract_id ? options.contract_id:''
    })
    that.getCompanyImg();
    //console.log('设备ID',options.equipment_id);
  },
  // 获取logo
  getCompanyImg: function () {
    var that = this;
    var company_id = app.globalData.company_id
    // ajax请求
    app.ajax({
      url: 'Api/Company/getCompayConfig',
      url_type: 1,
      data: { company_id: company_id },
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            logoImg: res.data.data.img_config.image
          })
        }
      }
    })
  },
  // 获取设备订单详情
  getEquipmentDetail: function () {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'Engineer/Equipmentlists/equipmentDetail',
      data: { equipment_id: that.data.equipment_id },
      success: function (res) {
        if (res.data.code == 1000) {
          // 数据赋值
          that.setData({
            equipmentDetail: res.data.data
          })
        }
        console.log('设备详情',res);
      }
    })
  },
  // 续费点击
  payment: function (e) {
    var that = this;
    console.log(12, that.data.coupon)
//    app.saveFormId(e.detail.formId);
    if (that.data.coupon_number > 0 && that.data.coupon.length <= 0){
      app.showToast('请选择抵扣券');
      return false;
    }
    // ajax请求添加续费订单
    app.ajax({
      url: 'Engineer/Renew/addRenewOrder',
      data: { 
        contract_id: that.data.contract_id,
        coupon: that.data.coupon,
        user_id: that.data.contractInfo.rental_user_info.user_id,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          wx.navigateTo({
            url: '/pages/subpackage/pages/renew/payment/payment?order_id=' + res.data.data.renew_order_id + '&type=2' + '&contract_id=' + that.data.contract_id+"&user_id="+that.data.contractInfo.rental_user_info.user_id,
          })
          // wx.removeStorageSync(coupon);
        }else{
          app.showToast(res.data.message);
        }
        console.log('续费下单', res)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    console.log("coupon获取", that.data.coupon)
    //that.getEquipmentDetail();
    that.getContractInfo();
   
    /*wx.getStorage({
      key: 'coupon',
      success(res) {
        console.log('coupon',res.data)
        that.setData({
          coupon: res.data
        })
      }
    })*/
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  //获取合同信息
  getContractInfo() {
    var that = this;
    app.ajax({
      url: 'Engineer/Contract/getContractInfo',
      method: "POST",
      data: {
        contract_id: that.data.contract_id
      },
      success: function (res) {
        console.log(that.data.coupon,"1111")
        if (res.data.code == 1000) {
          //计算续费金额
          var money = res.data.data.contract_info.renew_money * 100;
          if (util.isExitsVariable(res.data.data.coupon)){
             money = (res.data.data.contract_info.renew_money * 100) - that.data.coupon.length * (res.data.data.coupon * 100);
          }
          
          money = (money / 100).toFixed(2);
          that.setData({
            contractInfo: res.data.data,
            renew_money: money
          });
          that.getCouponList();
          
        }
      }
    })
  },
  //优惠券
  goCoupon(){
    var that = this;
    var couponStr = this.data.coupon.join('#');
    wx.navigateTo({
      url: '/pages/subpackage/pages/renew/econtractCoupon/econtractCoupon?coupon=' + couponStr + "&user_id=" + that.data.contractInfo.rental_user_info.user_id,
    })
  },
  // 获取我的优惠券列表
  getCouponList() {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'Engineer/Renew/getCoupon',
      data: {
        page: 1,
        row: 1,
        status:1,
        user_id: that.data.contractInfo.rental_user_info.user_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            coupon_number: res.data.data.page.total,
          })
        }
      }
    })
  },
})