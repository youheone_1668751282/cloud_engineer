// pages/subpackage//pages/crm/certification/certification.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSeachIcon: true,  //搜索_____________框搜索图标
    showCondition: false, //搜索_____________展示更多的搜索条件
    keyword: "",        //搜索_____________搜索桩号 
    startTime: "",        //搜索_____________开始时间
    endTime: "",          //搜索_____________结束时间
    typeList:[],                  //搜索_____________结束时间
    typeChose: '',//类型查询
    examine:[],
    examchose: '',

    dataList: [],
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getConfigList(1); //认证类型
    this.getConfigList(2); //审核状态
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    load = true;
    _PAGE = 1;
    that.getList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    _PAGE = 1
    that.getList();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getList();
  },

  //获取搜索配置列表
  getConfigList(type) {
    var that = this;
    var config = '';
    if (type == 1) { //认证类型
      config = 'auth_type';
    } else if (type == 2) { //审核状态
      config = 'realname_auth_status';
    }
    app.ajax({
      url: 'api/Customer/getSearchConfig',
      url_type: 2,
      data: {
        config_name: config
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (type == 1) { //认证类型
            that.setData({
              typeList: res.data.data
            })
          } else if (type == 2) { //审核状态
            that.setData({
              examine: res.data.data
            })
          }
        }
      }
    })
  },
  //获取实名认证列表
  getList() {
    var that = this;
    app.ajax({
      url: 'api/Realname/getLists',
      url_type: 2,
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
        key: that.data.keyword,
        startTime: that.data.startTime,
        endTime: that.data.endTime,
        type: that.data.typeChose,
        status: that.data.examchose
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              dataList: res.data.data,
              empty: false,
            })
          } else {
            that.setData({
              dataList: that.data.dataList.concat(res.data.data),
              empty: false
            });
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          load = false;
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    });
  },

  //审核
  navExamineEdit(e){
    wx.navigateTo({
      url: '../examineEdit/examineEdit?id=' + e.currentTarget.dataset.id,
    })
  },
  //查看
  navDetial(e) {
    wx.navigateTo({
      url: '../certificationInfo/certificationInfo?id=' + e.currentTarget.dataset.id,
    })
  },

  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    this.setData({
      startTime: "",
      endTime: ''
    })
    this.getList();
  },
  //搜索_____________搜索框选中
  searchFocus() {
    this.setData({
      showSeachIcon: false,
    })
  },
  //搜索_____________搜索输入框清除图标
  inputClear() {
    this.setData({
      keyword: '',
      showSeachIcon: true
    })
  },
  //搜索_____________搜索失去焦点
  searchBlur(e) {
    const result = e.detail.value;
    if (result === "") {
      this.setData({
        showSeachIcon: true,
      })
    }
  },
  //搜索_____________点击筛选
  tapFilter() {
    let showCondition = this.data.showCondition;
    this.setData({
      showCondition: !showCondition,
    })
  },
  //搜索_____________隐藏筛选条件
  hideFilter() {
    this.setData({
      showCondition: false,
    })
  },
  //搜索_____________重置搜索条件
  searchReset() {
    this.setData({
      startTime: "",
      endTime: '',
      keyword: "",
      typeChose:'',
      examchose:'',
    })
    this.getList();
  },
  //搜索_____________搜索确定
  searchConfirm() {
    this.setData({
      showCondition: false
    })
    this.getList();
  },
  //搜索_____________开始时间
  startDateChange: function (e) {
    this.setData({
      startTime: e.detail.value
    })
  },
  //搜索_____________开始时间
  endDateChange: function (e) {
    this.setData({
      endTime: e.detail.value
    })
  },
  //搜索_____________选择类型
  choseType: function (e) {
    this.setData({
      typeChose: e.currentTarget.dataset.chose
    })
  },
  //搜索_____________选择类型
  choseExamine: function (e) {
    this.setData({
      examchose: e.currentTarget.dataset.chose
    })
  },
})