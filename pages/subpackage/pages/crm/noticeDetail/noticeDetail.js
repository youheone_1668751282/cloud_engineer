// pages/wxParseDetail/wxParseDetail.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '',
    content: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.content)
    var that = this;
    var title = options.title || '';
    var content = options.content ? JSON.parse(decodeURIComponent(options.content)) : '';
    that.setData({
      title: title,
      content: content
    })
    if (title != '') {
      wx.setNavigationBarTitle({
        title: title,
      })
    }

    var article = content;
    WxParse.wxParse('article', 'html', article, that, 5);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  
  // 返回
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  }
})