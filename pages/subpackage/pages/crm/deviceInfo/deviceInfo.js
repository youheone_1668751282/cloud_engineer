// pages/subpackage/pages/crm/deviceInfo/deviceInfo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    equipment_id: '', //设备id
    dataInfo: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      equipment_id: options.equipment_id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取设备详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'api/Equipmentlists/getEquipmentlistsDetail',
      url_type: 2,
      data: {
        id: that.data.equipment_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            dataInfo: res.data.data
          })
        }
      }
    });
  },
})