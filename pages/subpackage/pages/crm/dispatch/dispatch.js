// pages/subpackage//pages/crm/dispatch/dispatch.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchValue: '',
    enginList: [],//工单列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading: false,
    work_order_id:'',
    change:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.work_order_id){
      this.setData({ work_order_id: options.work_order_id, change: options.change});
    }
  },
  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function () {
    pagea = 1;
    this.setData({
      enginList: [],
      is_load: false,
      hasMore: true,
    })
    this.getEngineerFun();//获取列表
  },
  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      searchValue: e.detail.value
    })
  },
  //搜索_____________点击搜索
  searchFun() {
    var that = this;
    pagea = 1;
    that.setData({
      enginList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getEngineerFun(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      enginList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getEngineerFun(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      enginList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getEngineerFun(); // 获取订单列表
  },
  // 获取工程人员列表
  getEngineerFun(){
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/Engineers/getEngineers',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        key: this.data.searchValue,
        work_order_id: that.data.work_order_id
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.enginList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          console.log('最后的数据', newlist)
          that.setData({
            enginList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false,
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },
  //选择项目
  assignFun(e){
    var that=this;
    var engineers_id=e.currentTarget.dataset.engineers_id;
    var change = that.data.change;
    var refeUrl='';
    if (change==0){
      refeUrl = 'api/Workorder/assignengineers';
    }else{
      refeUrl = 'api/Workorder/changeAssignengineers';
    }
    app.ajax({
      url: refeUrl,
      url_type: 2,
      data: {
        work_order_id: that.data.work_order_id,
        engineers_id: engineers_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          setTimeout(function(){
            app.showToast(res.data.msg);
              wx.navigateBack({
                delta: 1
              })
            },1300)
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
})