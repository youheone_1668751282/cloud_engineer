// pages/subpackage//pages/crm/defineSetMeal/defineSetMeal.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeList: [{ name: '时长模式', value: 0 }, { name: '流量模式', value: 1 }],
    tyepIndex: 0,//
    startTime: "", 
    endTime:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  //开始时间
  startDateChange: function (e) {
    this.setData({
      startTime: e.detail.value
    })
  },
  //到期時間
  endDateChange: function (e) {
    this.setData({
      endTime: e.detail.value
    })
  },
  // 选工单状态
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tyepIndex: e.detail.value
    })
  },
})