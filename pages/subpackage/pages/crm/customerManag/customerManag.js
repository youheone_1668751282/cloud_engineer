// pages/subpackage//pages/crm/customerManag/customerManag.js
var app = getApp();
var pagea = 1;
var pageSize=20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSeachIcon: true,  //搜索_____________框搜索图标
    showCondition: false, //搜索_____________展示更多的搜索条件
    keyword: "",        //搜索_____________搜索关键字
    startTime: "",        //搜索_____________开始时间
    endTime: "",          //搜索_____________结束时间
    areaList: [
      { name: '是有区域', value: '1' },
      { name: '没有区域', value: '2' }],//搜索_____________是否有区域
    havArea:'',
    customerList:[],//客户列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  onShow: function () {
    pagea=1;
    this.setData({
      customerList:[],
      is_load: false,
      hasMore: true,
    })
    this.getCustomer();//获取用户列表
  },

  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________搜索框选中
  searchFocus() {
    this.setData({
      showSeachIcon: false,
    })
  },
  //搜索_____________搜索输入框清除图标
  inputClear() {
    this.setData({
      keyword: '',
      showSeachIcon: true
    })
  },
  //搜索_____________搜索失去焦点
  searchBlur(e) {
    const result = e.detail.value;
    if (result === "") {
      this.setData({
        showSeachIcon: true,
      })
    }
  },
  //搜索_____________点击筛选
  tapFilter() {
    let showCondition = this.data.showCondition;
    this.setData({
      showCondition: !showCondition,
    })
  },
  //搜索_____________隐藏筛选条件
  hideFilter() {
    this.setData({
      showCondition: false,
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    pagea = 1;
    this.setData({
      customerList: [],
      startTime: "",
      endTime: '',
      loading: true,
      havArea: '',
      is_load: false
    })
    this.getCustomer();//获取用户列表
  },
  //搜索_____________重置搜索条件
  searchReset() {
    var areaList = this.data.areaList;
    areaList.forEach(function (item, index) {
      item.checked = false;
    });
    this.setData({
      startTime: "",
      endTime: '',
      keyword: "",
      areaList: areaList,
      havArea:''
    })
  },
  //搜索_____________搜索确定
  searchConfirm() {
    pagea = 1;
    this.setData({
      showCondition: false,
      loading: true,
      customerList: [],
      is_load: false
    })
    this.getCustomer();//获取用户列表
  },
  //搜索_____________开始时间
  startDateChange: function (e) {
    this.setData({
      startTime: e.detail.value
    })
  },
  //搜索_____________开始时间
  endDateChange: function (e) {
    this.setData({
      endTime: e.detail.value
    })
  },
  //搜索_____________是否有区域
  radioChange: function (e) {
    var areaList=this.data.areaList;
    var havArea = e.detail.value;
    areaList.forEach(function (item, index) {
      if(item.value == e.detail.value){
        item.checked = true;
      }else{
        item.checked = false;
      }
    });
    this.setData({
      areaList,
      havArea
    })
  },

  //跳转客户编辑
  navCustomerEdit(e){
    //showtype 对应的值1查看  2新增  3编辑
    wx.navigateTo({
      url: '../customerEdit/customerEdit?user_id=' + e.currentTarget.dataset.user_id + '&showtype=' + e.currentTarget.dataset.showtype,
    })
  },
  //跳转交换
  navExchange(e) {
    wx.navigateTo({
      url: '../exchange/exchange?way=1&user_id=' + e.currentTarget.dataset.user_id,
    })
  },
  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getCustomer(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      customerList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getCustomer(); // 获取订单列表
  },
  // 获取订单列表
  getCustomer: function () {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/Customer/getLists',
      url_type: 2,
      data: { 
        page: pagea, 
        pageSize: pageSize,
        key: this.data.keyword,
        administrative_id: this.data.havArea,
        startTime: this.data.startTime,
        endTime: this.data.endTime
        },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.customerList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          console.log('最后的数据', newlist)
          that.setData({
            customerList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false,
        })
        
        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },
})