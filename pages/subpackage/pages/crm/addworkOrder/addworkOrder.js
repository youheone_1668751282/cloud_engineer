// pages/subpackage//pages/crm/addworkOrder/addworkOrder.js
var app = getApp();
var formUp=true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    serviceType: [],//业务类型
    businessid:[],//提交的业务类型
    tyepIndex: 0,//
    collectMoneyList: [{ name: '是', value: 1, checked: 'true'}, { name: '否', value: 2,  }],//是否收费用
    housetList: [{ name: '购买', value: 1, checked: 'true' }, { name: '公租房', value: 2 }, { name: '租赁', value: 3 }],//购房性质
    outList: [{ name: '是', value: 1 }, { name: '否', value: 2, checked: 'true' }],//是否外接
    transferList: [{ name: '是', value: 1 }, { name: '否', value: 2, checked: 'true' }],//是否新建移机工单
    isOutserver:false,//是否是外接订单
    seeList: [{ name: '是', value: 1, checked: 'true' }, { name: '否', value: 2}],//是否对用户可见
    add_ress: '', //详细地址
    ch_latitude: '', //当前
    ch_longitude: '', //当前
    service_address:'',//移机地址
    chNew_latitude:'',//移机
    chNew_longitude:'',//移机
    valueName: [],//设置回显选择的地址(name)
    getAreaMsg: '',//选择的地址信息
    moveAMsg: '',//移机新装地址
    cusDetail:'',//选择客户的信息
    user_id: '',    //选择--用户--ID
    user_name: '',  //选择--用户--姓名
    device_no:'',   //选择--设备--编号
    equipment_id:'',//选择--设备--ID
    havAddorder:false,//是否有新增工单(此处仅为了展示提示))
    isNewClothes:false,//是否是新装工单
    isRepair:false,    //是否是维修工单
    isCore:false,      //是否是换芯工单
    isDemolition:false,//是否是拆机工单
    isRelocation:false,//是否是移机工单
    isUrge:false,      //是否是催费工单  
    pac_sign_date: '',  //自定义装机时间
    pac_expire_date: '',//自定义到期时间
    explainList: [{ id: '', name: "请选择讲解人", phone: "" }],//讲解人列表
    explainIndex:0,
    produceList: [{ equipments_id: '', equipments_name:'请选择产品名称'}],
    produceIndex: 0,//产品名称
    produceNewIndex: 0,//新产品
    packageList: [{ package_id: '', package_name:'请选择套餐'}],
    packageIndex:0,//设备列表
    problemList: [{ problem_id: '', problem_desc: '请选择主要问题' }],//
    problemIndex:0,//问题列表
    coresList:[],//换芯套餐列表
    coreIndex:0,//换芯套餐选择
    corePacks_id:'',//当前选中的换芯套餐ID
    coreChangeList:[],//换芯列表
    filterList:[],//选择滤芯列表
    package_type:1,//是否是自定义套餐 1非自定义 2自定义
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 初始化动画变量
    var that = this;
    that.headidnew = this.selectComponent("#addressNewid");
    that.headid = this.selectComponent("#addressid"); //引入地址
   
    that.getBusinessType();//获取工单业务类型
    that.getProductList();//获取产品列表
    that.getProblemsList();//获取主要问题列表
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (this.data.user_id) {
      console.log('选择的用户信息', this.data.user_id, this.data.user_name)
      this.getCustomerDetail(this.data.user_id);//获取选中用户详情
    }
    if (this.data.equipment_id){
      this.getFilterDetails(this.data.equipment_id, this.data.corePacks_id);
    }
  },
  //跳转客户列表选择
  navExchange() {
    wx.navigateTo({
      url: '../exchange/exchange?way=2',//交换也可进客户列表页面 交换1 新增工单传的2
    })
  },
  //自定义列表
  defineSetMeal(){
    wx.showModal({
      title: '该功能正在开发中',
      content: '请移驾PC端使用该功能',
      showCancel:false,
      confirmColor:'rgba(108,169,255,1)'
    })
    return false;
    //
    wx.navigateTo({
      url: '../defineSetMeal/defineSetMeal',//自定义套餐
    })
  },
  //跳转客户列表选择 
  deviceManage() {
    if(!this.data.user_id){
        app.showToast('请先选择用户信息');
        return false
    }
    wx.navigateTo({
      url: '../deviceManage/deviceManage?chose=true&userid='+this.data.user_id,//交换也可进客户列表页面 交换1 新增工单传的2
    })
  },
  //选择业务类型
  serviceTypeChange(e) {
    const that=this;
    const serviceType = that.data.serviceType;
    const values = e.detail.value;
    const lenJ = values.length;
    //判断是否是新装工单(其他类型则不能选择)反之也如此,values[0]==3新装工单只能独立选择(根据原来逻辑来写的);
    if (values[0]==3){
      serviceType.forEach(function (item, i) {
        if (item.business_type_id==3){
          item.disabled = false;
          item.checked = true;
          that.setData({ isNewClothes: true, isUrge: false, isCore: false, isDemolition: false, isRelocation: false, isUrge: false});//除了新装工单
        }else{
          item.disabled = true;
        }
      })
    }else{
      serviceType.forEach(function (item, i) {
        item.checked = false;
        if (item.business_type_id == 3) {
           item.disabled = true;
          if (lenJ<=0){
            item.disabled = false;
            that.setData({ isNewClothes: false});//不是新装
          }
          item.checked = false;
        } else {
          //that.setData({ isNewClothes: false, isUrge: false, isCore: false, isDemolition: false, isRelocation: false, isUrge:false});//不是新装
          item.disabled = false;//重置所有 之后再有的值中循环选中
          values.forEach(function(its,k){
            if (item.business_type_id == its) {
              item.checked = true;
              if(its==1){
                that.setData({ isUrge:true});
              } else if (its == 2) {
                that.setData({ isCore: true });
              } else if (its == 4) {
                that.setData({ isDemolition: true });
              } else if (its == 5) {
                that.setData({ isRelocation: true });
              } else if (its == 6) {
                that.setData({ isRepair: true });
              }
            }else{
              item.disabled = true;
              console.log('数据结果', item.business_type_id, its, item.checked);
              if (!item.checked){
                if (item.business_type_id == 1) {
                  that.setData({ isUrge: false });
                } else if (item.business_type_id == 2) {
                  that.setData({ isCore: false });
                } else if (item.business_type_id == 4) {
                  that.setData({ isDemolition: false });
                } else if (item.business_type_id == 5) {
                  that.setData({ isRelocation: false });
                } else if (item.business_type_id == 6) {
                  that.setData({ isRepair: false });
                } 
              }
            }
          });
        }
      })
      if (lenJ <= 0) {
        that.setData({ isNewClothes: false, isRepair: false, isCore: false, isDemolition: false, isRelocation: false, isUrge: false });//都不选
      }  
    }
    //serviceType  values
    // 获取所有id集合 觉得这种方式要简单些,但是不想改了
    //let arr1Ids = values.map(item => Number(item));
    //const result = serviceType.filter(item => !arr1Ids.includes(item.business_type_id));//不同的
    //const result2 = serviceType.filter(item => arr1Ids.includes(item.business_type_id));//相同的
    //console.log('不同的', result, '相同', result2, arr1Ids); 
    console.log('工单业务类型', serviceType, values);
    this.setData({
      serviceType,
      businessid: values
    })
  },
  //搜索_____________开始时间
  startDateChange: function (e) {
    this.setData({
      pac_sign_date: e.detail.value
    })
  },
  //搜索_____________开始时间
  endDateChange: function (e) {
    this.setData({
      pac_expire_date: e.detail.value
    })
  },
  //选择是否外接
  radioChange1: function (e) {
    var outList = this.data.outList;
    outList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      outList,
      isOutserver: e.detail.value==1?true:false
    })
  },
  //是否新建移机装工单
  radioTransferChange: function(e) {
    var transferList = this.data.transferList;
    transferList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      transferList
    })
  },
  //是否收租赁费用
  collectMoneyChange(e){
    var collectMoneyList = this.data.collectMoneyList;
    collectMoneyList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      collectMoneyList
    })
  },
  //住房性质选择
  houseTypeChange(e){
    var housetList = this.data.housetList;
    housetList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      housetList
    })
  },
  //是否對用戶可见
  isSeeChange: function (e) {
    var seeList = this.data.seeList;
    seeList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      seeList
    })
  },
  // 选工单状态
  bindPickerChange: function (e) {
    this.setData({
      tyepIndex: e.detail.value
    })
  },
  //选择讲解人
  bindExplainChange:function(e){
    this.setData({
      explainIndex: e.detail.value
    })
  },
  //选择产品列表
  bindProduceChange: function (e) {
    this.setData({
      produceIndex: e.detail.value,
      packageIndex: 0
    })
    if (e.detail.value!=0){
      this.getPackagesList(e.detail.value);//获取新装产品套餐
    }
  },
  //选择新产品列表(移机中改变的)
  bindProduceNewChange: function (e) {
    this.setData({
      produceNewIndex: e.detail.value
    })
  },
  //选择套餐列表
  bindPackageChange: function (e) {
    console.log('bindPackageChange', this.data.packageList);
    console.log('bindPackageChange', this.data.packageIndex, e.detail.value);
    this.setData({
      packageIndex: e.detail.value
    })
  },
  //常见问题列表  
  bindProblemChange: function (e) {
    this.setData({
      problemIndex: e.detail.value
    })
  },
  //换芯套餐
  bindCoreChange: function (e) {
    var that = this, coresList = that.data.coresList, corePacks_id='';
    coresList.forEach(function (item, index) {
      if (item.id == e.detail.value) {
        item.checked = true;
        corePacks_id=item.id
        that.getFilterDetails(that.data.equipment_id, corePacks_id);
      } else {
        item.checked = false;
      }
    });
    that.setData({
      coresList,
      corePacks_id
    })
  },
  //选择换芯列表
  bindFilterChange(e){
    var index = e.currentTarget.dataset.index, value = e.detail.value;
    var coreChangeList = this.data.coreChangeList;
    coreChangeList[index].filter_index = value;
    coreChangeList[index].filter_element_id = this.data.filterList[value].parts_id;
    coreChangeList[index].filter_element_day = this.data.filterList[value].cycle;
    console.log('选择之后的数据', coreChangeList);
    this.setData({
      coreChangeList,
    })
  },
  //------------添加滤芯
  addCoreArr:function(){
    if (this.data.coreChangeList.length>=5){
      app.showToast('最多只能添加五条!'); return false;
    }
    var coreChangeList = this.data.coreChangeList;
    coreChangeList.push({ filter_element_day: '', filter_index: 0, filter_element_id: '' });
    this.setData({
      coreChangeList
    })
  },
  //------------删除滤芯
  detalCoreFun:function(e){
    var coreindex = e.currentTarget.dataset.coreindex,coreChangeList = this.data.coreChangeList;
    coreChangeList.splice(coreindex, 1);
    this.setData({
      coreChangeList
    })
  },
  //输入换芯天数
  inputCoreDay:function(e) {
    var coreindex = e.currentTarget.dataset.coreindex, value = e.detail.value, coreChangeList = this.data.coreChangeList;
    coreChangeList[coreindex].filter_element_day = value;
    console.log('输入', coreChangeList)
    this.setData({
      coreChangeList
    })
  },
  //打开地图选择 
  openAddress() {
    var that = this;
    this.headid.startAddressAnimation(true);
  },
  //确定接受
  getSure(data) {
    console.log('确定事件获取地址', data);
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    that.setData({
      getAreaMsg: data.detail
    })
  },
  //打开地图选择 
  openNewAddress() {
    var that = this;
    this.headidnew.startAddressAnimation(true);
  },
  //确定接受
  getNewSure(data) {
    console.log('确定事件获取地址', data);
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    that.setData({
      moveAMsg: data.detail
    })
  },
  
  //选择地址
  openAeraFun(){
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        console.info(res);
        that.setData({
          add_ress: res.name,
          ch_latitude: res.latitude,
          ch_longitude: res.longitude,
        })
      },
    })
  },

  //选择地址(移机)
  openAeraNewFun() {
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        console.info(res);
        that.setData({
          service_address: res.name ? res.name : res.address,
          chNew_latitude: res.latitude,
          chNew_longitude: res.longitude,
        })
      },
    })
  },
  
  //获取工单业务类型
  getBusinessType() {
    var that = this;
    app.ajax({
      url: 'api/Workorder/getBusinessType',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var serviceType = res.data.data;
          serviceType.forEach(function (item, index) {
              item.checked = false;
              item.disabled = false;
          });
          that.setData({
            serviceType
          })
        } else {
          app.showToast(res.data.msg);
        }
        that.getExplainers();//获取讲解人列表
      }
    })
  },
  //获取选中的客户信息
  getCustomerDetail(id) {
    var that = this;
    app.ajax({
      url: 'api/Customer/getCustomerDetail',
      url_type: 2,
      data: {
        user_id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var detialMsg=res.data.data;
          that.setData({
            cusDetail: detialMsg,
          })
          that.statisticsworkordernum(id);//判断是否有新增登工单
        } else {
          app.showToast(res.data.msg);
        }
        
      }
    })
  },
  //判断是否有新增登工单
  statisticsworkordernum(id) {
    var that = this;
    app.ajax({
      url: 'api/workorder/statisticsworkordernum',
      url_type: 2,
      data: {
        business_id: 3,
        user_id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            havAddorder: res.data.data==1?true:false
          })
        } else {
          app.showToast(res.data.msg);
        }

      }
    })
  },
  //获取讲解人列表 
  getExplainers(){
      var that=this;
    app.ajax({
      url: 'api/Workorder/getExplainers',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var explainList = that.data.explainList;
          explainList = explainList.concat(res.data.data);
          that.setData({
            explainList
          })
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //获取产品列表
  getProductList() {
    var that = this;
    app.ajax({
      url: 'api/Equipments/getList',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var produceList = that.data.produceList;
          produceList = produceList.concat(res.data.data);
          that.setData({
            produceList
          })
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //获取新装套餐列表
  getPackagesList(idx){ 
    var that = this;
    var id = that.data.produceList[idx].equipments_id;
    app.ajax({
      url: 'api/Equipments/getPackage',
      url_type: 2,
      data: {
        equipments_id:id
      },
      success: function (res) {
        var packageList = [{ package_id: '', package_name: '请选择套餐' }];
        if (res.data.code == 1000) {
          
          packageList = packageList.concat(res.data.data);
          that.setData({
            packageList
          })
        } else {
          packageList=[];
          // app.showToast(res.data.msg);
          that.setData({
            packageList
          })
        }
      } 
    })
  }, 
  // 获取问题列表 
  getProblemsList() {
    var that = this;
    app.ajax({
      url: 'api/Workorder/getProblem',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var problemList = that.data.problemList;
          problemList = problemList.concat(res.data.data);
          that.setData({
            problemList
          })
        } else {
          app.showToast(res.data.msg);
        }
        that.getFiltersPackage();
      }
    })
  }, 
  //获取换芯列表 
  getFiltersPackage() {
    var that = this;
    app.ajax({
      url: 'api/Workorder/getFilterPackage',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var coresList = res.data.data, corePacks_id='';
          coresList.forEach(function (item, index) {
            if (index==0){
              item.checked = true;
              corePacks_id=item.id;
            }else{
              item.checked = false;
            }
          });
          that.setData({
              coresList,
            corePacks_id
          })
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  }, 
  //获取换芯列表
  getFilterDetails(equipment_id, corePacks_id) {
    var that = this;
    app.ajax({
      url: 'api/Parts/getFilterDetail',
      url_type: 2,
      data: {
        equipment_id: equipment_id,
        package_id:corePacks_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var coreChangeList = [], filterList = [{ parts_name: "请选择套餐", parts_id: 0, cycle: 0}];
          filterList = filterList.concat(res.data.data);
          var obj = { filter_element_day: '', filter_index: 0, filter_element_id:''};
          coreChangeList.push(obj);
          that.setData({
            coreChangeList,
            filterList,
          })
          console.log('处理之后的数据', coreChangeList)
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  }, 
  // 检测表单
  checkForm(isTrue, message) {
    if (isTrue) {
      return { isReturn: isTrue, message }
    } else {
      return false;
    }
  },
  // 提交表单
  saveForm(e) {
    let formData = e.detail.value;
    let { isReturn, message } = this.checkForm(formData.account == '', '请输入账号') || this.checkForm(formData.password == '', '请输入密码');
    if (isReturn) {
      app.showToast(message);
      return;
    }
  },
  //取消
  cancelFun(){
    wx.navigateBack({
      delta:1
    })
  },
  //提交的表单数据 api/Workorder/add
  formSubmit(e) {
    console.log('表单数据', e.detail.value);
    // return false;
    var that = this, pageData = that.data, cusDetail = that.data.cusDetail, formData = e.detail.value;
    var explainData = pageData.explainList[pageData.explainIndex];
    var packageData = pageData.packageList[pageData.packageIndex];
    var problemData = pageData.problemList[pageData.problemIndex];
    var produceData = pageData.produceList[pageData.produceIndex];
    var newprduData = pageData.produceList[pageData.produceNewIndex];

    let { isReturn, message } = 
      that.checkForm(cusDetail == '', '请先选择用户信息') || 
      that.checkForm(formData.contacts == '', '联系人不能为空') || 
      that.checkForm(formData.contactsPhone == '', '联系人电话不能为空') || 
      that.checkForm(pageData.businessid.length <= 0, '请先选择业务类型') || 
      that.checkForm(cusDetail == '', '请选择用户信息');
    if (isReturn) {
      app.showToast(message);
      return;
    }
    // 新装
    if (pageData.isNewClothes && !pageData.isOutserver){
      let { isReturn, message } = 
        that.checkForm(pageData.produceIndex == 0 && pageData.isNewClothes, '请选择产品')||
        that.checkForm(pageData.packageIndex == 0 && pageData.packageList.length > 0, '请选择套餐')||
        that.checkForm(pageData.packageIndex == 0 && pageData.packageList.length > 0, '请选择套餐')||
        that.checkForm(pageData.getAreaMsg.length==0,'请选安装地址')||
        that.checkForm(pageData.ch_latitude == '' ||pageData.ch_longitude == '' ||formData.user_address=='', '请先定位位置信息');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }
    //维修 外接
    if (pageData.isRepair && !pageData.isOutserver){
      let { isReturn, message } = 
        that.checkForm(pageData.equipment_id == '', '请选择设备信息') ||
        that.checkForm(problemData.problem_id=='', '请选择主要问题') ||
        that.checkForm(formData.problem=='', '请输入问题描述');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }
    //换芯  外接   coreFilterID当前选中滤芯id列表  coreFilterDAY当前选中滤芯Day列表
    var coreChangeList = this.data.coreChangeList, coreFilterID = [], coreFilterDAY = [];
    if (pageData.isCore && !pageData.isOutserver) {
      coreChangeList.forEach(function (item, i) {
        if (item.filter_element_id) {
          coreFilterID.push(item.filter_element_id);
          coreFilterDAY.push(item.filter_element_day);
        }
      })
      let newOnw = coreFilterDAY.find((value, index, arr) => {
        return value == 0  
      })
      let { isReturn, message } =
        that.checkForm(pageData.equipment_id == '', '请选择设备信息') ||
        that.checkForm(coreFilterID.length == 0, '请选择换芯套餐') ||
        that.checkForm(coreChangeList.length != coreFilterID.length, '请完善换芯套餐')||
        that.checkForm(newOnw == 0, '换芯周期不能为0天');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }
    //拆机
    if (pageData.isDemolition && !pageData.isOutserver) {
      let { isReturn, message } =
        that.checkForm(pageData.equipment_id == '', '请选择设备信息');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }

    //移机 外接
    if (pageData.isRelocation && !pageData.isOutserver) {
      let { isReturn, message } =
        that.checkForm(pageData.equipment_id == '', '请选择设备信息') ||
        that.checkForm(newprduData.equipments_id == '', '请选择新产品名称') ||
        that.checkForm(pageData.moveAMsg.length == 0, '请选新安装地址') ||
        that.checkForm(pageData.chNew_latitude == ''||pageData.chNew_longitude == ''||formData.service_address=='','请先定位位置信息');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }

    //催费
    if (pageData.isUrge && !pageData.isOutserver) {
      let { isReturn, message } =
        that.checkForm(pageData.equipment_id == '', '请选择设备信息');
      if (isReturn) {
        app.showToast(message);
        return;
      }
    }
    var submitData={
      package_type: pageData.package_type,
      _username: formData._username,
      user_id: cusDetail.user_id,
      equipments_type: '',
      contacts: formData.contacts,
      contact_number: formData.contactsPhone,
      businessid: pageData.businessid,
      explainer: explainData.id,
      equipments_id: produceData.equipments_id,
      equipmentsname: produceData.equipments_id,//这是 id
      pac_name: pageData.packageIndex == 0 ? '' : packageData.package_name,
      pac_mode: pageData.packageIndex == 0 ? '': packageData.mode,
      pac_num: pageData.packageIndex == 0 ? '' : packageData.num,
      pac_total_money: pageData.packageIndex == 0 ? 0 : packageData.total_money,
      pac_renew_money: pageData.packageIndex == 0 ? 0 : packageData.renew_money,
      pac_cycle: pageData.packageIndex == 0 ? '' : packageData.cycle,
      pac_expire_date: '',//自定义到期时间
      pac_sign_date: '',//自定义装机时间
      pac_service_new_money: pageData.packageIndex == 0 ? 0 : packageData.new_service_money,
      pac_service_renew_money: pageData.packageIndex == 0 ? 0 : packageData.renew_service_money,
      pac_new_service_money_ratio: pageData.packageIndex == 0 ? '' : packageData.new_service_money_ratio,
      pac_renew_service_money_ratio: pageData.packageIndex == 0 ? '' : packageData.renew_service_money_ratio,
      package: pageData.packageIndex == 0 ? '' : packageData.package_id,
      equipment_num: Number(formData.equipment_num),
      is_collect_money: formData.is_collect_money,
      is_circumscribed: formData.is_circumscribed,
      is_new_word_order: formData.is_new_word_order,
      equipment_id: pageData.equipment_id,
      device_no: pageData.device_no,
      filter_package: pageData.corePacks_id,
      filter_element:coreFilterID,
      filter_element_day: coreFilterDAY,
      question: [problemData.problem_id]||[],
      questionMore: formData.problem,
      moveprovince: pageData.moveAMsg?pageData.moveAMsg.areaName[0]:'', 
      movecity: pageData.moveAMsg ? pageData.moveAMsg.areaName[1] : '', 
      movearea: pageData.moveAMsg ? pageData.moveAMsg.areaName[2] : '', 
      province_id: pageData.moveAMsg?pageData.moveAMsg.areaId[0]:'',
      city_id: pageData.moveAMsg ? pageData.moveAMsg.areaId[1] : '',
      area_id: pageData.moveAMsg ? pageData.moveAMsg.areaId[2] : '',
      moveEquipmentsName: newprduData.equipments_id||'',//这是移机
      service_address: formData.service_address,
      house_type: formData.house_type,
      userprovince: pageData.getAreaMsg?pageData.getAreaMsg.areaName[0]:'',
      usercity: pageData.getAreaMsg ?pageData.getAreaMsg.areaName[1]:'',
      userarea: pageData.getAreaMsg?pageData.getAreaMsg.areaName[2]:'',
      province_code: pageData.getAreaMsg?pageData.getAreaMsg.areaId[0]:'',
      city_code: pageData.getAreaMsg ?pageData.getAreaMsg.areaId[1]:'',
      area_code: pageData.getAreaMsg ?pageData.getAreaMsg.areaId[2]:'',
      user_address: formData.user_address,
      is_to_user: formData.is_to_user,
      remarks: formData.remarks,
      lat: pageData.ch_latitude,
      lng: pageData.ch_longitude,
      move_lat: pageData.chNew_latitude,
      move_lng: pageData.chNew_longitude,
    }

    console.log('提交的表单数据', submitData);
    if (!formUp) {
      app.showToast('请勿重复提交该表单');
      return false;
    }
    formUp = false;
    app.ajax({
      url: 'api/Workorder/add',
      url_type: 2,
      data: submitData,
      success: function (res) {
        if (res.data.code == 1000) {
        
          app.showToast(res.data.msg);
          setTimeout(function () {
            wx.navigateBack({
              delta: 1
            })
          }, 1000);
        } else {
          app.showToast(res.data.msg);
        }
      
      setTimeout(function(){
        formUp = true;
      },600);
      }
    })
  },

})