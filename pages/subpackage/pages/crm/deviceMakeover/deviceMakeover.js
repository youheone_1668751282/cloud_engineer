// pages/subpackage/pages/crm/deviceMakeover/deviceMakeover.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_id: '',//选择的客户id
    user_name: '',//选择的客户名称
    device_id: '',//选择的设备id
    device_name: '',//选择的设备名称
    user_id_m: '',//选择的被转让人id
    user_name_m: '',//选择的被转让人名称
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
    var that = this;
    if (!this.data.user_id) {
      app.showToast('请选择用户');
      return
    } else if (!this.data.device_id) {
      app.showToast('请选择用户要转让的设备');
      return
    } else if (!this.data.user_id_m) {
      app.showToast('请选择被转让人');
      return
    }
    app.ajax({
      url: 'api/Device/add_transfer',
      url_type: 2,
      data: {
        users_id: that.data.user_id,
        equipments_id: that.data.device_id,
        customer_id: that.data.user_id_m,
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //重置表单
  formReset(){
    this.setData({
      user_id: '',//选择的客户id
      user_name: '',//选择的客户名称
      device_id: '',//选择的设备id
      device_name: '',//选择的设备名称
      user_id_m: '',//选择的被转让人id
      user_name_m: '',//选择的被转让人名称
    })
  },
  //选择用户
  navCustomerList(e) {
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '../exchange/exchange?way=' + type,
    })
  },
  //选择设备
  navDeviceList(){
    if (!this.data.user_id){
      app.showToast('请先选择用户');
      return
    } else {
      wx.navigateTo({
        url: '../deviceManage/deviceManage?chose=true&way=1&userid=' + this.data.user_id,
      })
    }
  },
})