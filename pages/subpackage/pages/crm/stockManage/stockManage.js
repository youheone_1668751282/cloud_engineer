// pages/user/stockManage/stockManage.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyword: '',//关键字搜索
    stocksList: '',//库存列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function (options) {
    pagea = 1;
    this.setData({
      stocksList: [],
      is_load: false,
      hasMore: true,
    })
    this.getStockList();//获取列表
  },

  //搜索_____________只搜索编号
  onlyKeywordber() {
    pagea = 1;
    this.setData({
      stocksList: [],
      is_load: false,
      hasMore: true,
    })
    this.getStockList();
  },
  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //变动记录
  stockRecord() {
    wx.navigateTo({
      url: '../stockRecord/stockRecord',
    })
  },
  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getStockList(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      stocksList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getStockList(); // 获取订单列表
  },
  //获取库存
  getStockList() {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/Equipments/getStockLists',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        key: that.data.keyword
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.stocksList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          // console.log('最后的数据', newlist)
          that.setData({
            stocksList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },
  //增减
  numChange(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/crm/numChange/numChange?id='+id,
    })
  },
  //规划
  transfer(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/crm/transfer/transfer?id=' + id,
    })
  },
  //变动记录
  changeRecord(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/crm/changeRecord/changeRecord?id=' + id,
    })
  }


})