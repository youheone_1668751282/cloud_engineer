// pages/subpackage//pages/crm/numChange/numChange.js
var stock_id='';
var app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    stock_id = options.id ? options.id:'';
  },
  //库存数量增减
  formSubmit(e) {
    // console.log('数量',e);
    var that = this;
    var change_volume = e.detail.value.nums;
    var remark = e.detail.value.remarks;
    app.ajax({
      url: 'api/Equipments/change_self',
      url_type: 2,
      data: {
        stock_id,
        change_volume,
        remark,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.msg);
          setTimeout(()=>{
            wx.navigateBack({
              delta: 1,
            })
          },1000)
        }else{
          app.showToast(res.data.msg);
        }
      }
    })
  },
})