// pages/subpackage/pages/crm/engineerConfigure/engineerConfigure.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    limits: '',//上门范围
    limits_status: [{ name: '开启', value: 1, checked: 'true' }, { name: '关闭', value: 0 }],//是否开启上门范围限制 1开启 0关闭
    cash_receipt: [{ name: '开启', value: 1, checked: 'true' }, { name: '关闭', value: 0 }],//现金收款
    transfer_accounts: [{ name: '开启', value: 1, checked: 'true' }, { name: '关闭', value: 0 }],//对公转账
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getEngineerConfigInfo();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //获取配置信息
  getEngineerConfigInfo() {
    var that = this;
    app.ajax({
      url: 'api/Config/getEngineerConfigInfo',
      url_type: 2,
      data: {},
      success: function (res) {
        // 处理现金收款回显 cash_receipt
        const cash_receipt = that.data.cash_receipt;
        cash_receipt.forEach(function (item, index) {
          if (item.value == res.data.data.enginer_cash_pay) {
            item.checked = true;
          } else {
            item.checked = false;
          }
        });
        // 处理上门范围限制回显 limits_status
        const limits_status = that.data.limits_status;
        limits_status.forEach(function (item, index) {
          if (item.value == res.data.data.staff_distance_limit_switch) {
            item.checked = true;
          } else {
            item.checked = false;
          }
        });
        // 处理对公转账回显 transfer_accounts
        const transfer_accounts = that.data.transfer_accounts;
        transfer_accounts.forEach(function (item, index) {
          if (item.value == res.data.data.enginer_transfer_pay) {
            item.checked = true;
          } else {
            item.checked = false;
          }
        });
        that.setData({
          limits: res.data.data.staff_door_to_door,
          limits_status: limits_status,
          cash_receipt: cash_receipt,
          transfer_accounts: transfer_accounts,
        })
      }
    })
  },

  //选择上门范围限制开关
  radioLimitsStatus(e) {
    var limits_status = this.data.limits_status;
    limits_status.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      limits_status
    })
  },
  //选择是否开启现金收款
  radioCashReceipt(e) {
    var cash_receipt = this.data.cash_receipt;
    cash_receipt.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      cash_receipt
    })
  },
  //选择是否开启对公转账
  radioTransferAccounts(e) {
    var transfer_accounts = this.data.transfer_accounts;
    transfer_accounts.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      transfer_accounts
    })
  },

  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
    var that = this;
    var value = e.detail.value;
    // if (value.limits == '') {
    //   app.showToast('请输入上门范围');
    //   return
    // }
    app.ajax({
      url: 'api/Config/addEngineerConfig',
      url_type: 2,
      data: {
        // staff_door_to_door: value.limits,
        // staff_distance_limit_switch: value.limits_status,
        // enginer_transfer_pay: value.transfer_accounts,
        staff_door_to_door: '',
        staff_distance_limit_switch: '',
        enginer_transfer_pay: '',
        enginer_cash_pay: value.cash_receipt,
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //表单重置
  formReset(){
    this.getEngineerConfigInfo();
  },
})