// pages/subpackage/pages/crm/deviceReplaceList/deviceReplaceList.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyword: "",          //搜索_____________搜索关键字内容

    dataList: [],
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
  },

  /*** 生命周期函数--监听页面加载*/
  onLoad: function (options) {
    
  },
  /*** 生命周期函数--监听页面显示*/
  onShow: function () {
    var that = this;
    load = true;
    _PAGE = 1;
    that.setData({
      dataList: [],
    });
    that.getList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    _PAGE = 1
    that.getList();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getList();
  },

  //获取设备替换记录
  getList() {
    var that = this;
    app.ajax({
      url: 'api/Equipmentlists/deviceReplaceLog',
      url_type: 2,
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
        key: that.data.keyword,
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              dataList: res.data.data,
              empty: false,
            })
          } else {
            that.setData({
              dataList: that.data.dataList.concat(res.data.data),
              empty: false
            });
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          load = false;
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    });
  },

  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    this.getList();
  },
  //跳转替换设备
  navDeviceReplace() {
    wx.navigateTo({
      url: '../deviceReplace/deviceReplace',
    })
  },
})