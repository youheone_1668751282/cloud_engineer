// pages/subpackage//pages/crm/exchange/exchange.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    way: 1,//跳转过来的方式，1交换，2工单选择、报备选择、设备转让选择用户，3添加客户，4选择设备被转让人
    searchValue:'',
    user_id: '',

    customerList: [],//客户列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      way: options.way,
      user_id: options.user_id
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    pagea = 1;
    this.setData({
      customerList: [],
      is_load: false,
      hasMore: true,
    })
    this.getCustomer();//获取用户列表
  },

  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getCustomer(); // 获取订单列表
  },
  
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      customerList: [], // 客户列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getCustomer(); // 获取客户列表
  },

  // 获取客户列表
  getCustomer: function () {
    var that = this;
    app.ajax({
      url: 'api/Customer/getLists',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        key: that.data.searchValue,
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.customerList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          console.log('最后的数据', newlist)
          that.setData({
            customerList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false,
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },

  //跳转客户添加
  navCustomerEdit(e) {
    //showtype 对应的值1查看  2新增  3编辑
    wx.navigateTo({
      url: '../customerEdit/customerEdit?user_id=' + e.currentTarget.dataset.user_id + '&showtype=' + e.currentTarget.dataset.showtype,
    })
  },

  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      searchValue: e.detail.value
    })
  },
  //搜索_____________点击搜索
  searchFun(){
    pagea = 1;
    this.setData({
      customerList: [],
      loading: true,
      is_load: false
    })
    this.getCustomer();//获取用户列表
  },

  //交换手机号
  exchangePhone(e){
    var that = this;
    app.ajax({
      url: 'api/Customer/exchangeTel',
      url_type: 2,
      data: {
        //user_id: that.data.user_id,
        //exchange_userid: e.currentTarget.dataset.user_id,
        user_id: e.currentTarget.dataset.user_id,
        exchange_userid: that.data.user_id,
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 2000)
          }
        })
      }
    })
  },
  //交换认证信息
  exchangeInfo(e) {
    var that = this;
    app.ajax({
      url: 'api/Customer/exchangeAuth',
      url_type: 2,
      data: {
        //user_id: that.data.user_id,
        //exchange_userid: e.currentTarget.dataset.user_id,
        user_id: e.currentTarget.dataset.user_id,
        exchange_userid: that.data.user_id,
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //确认选择，返回上一页
  chooseSure(e){
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1]; // 当前页面
    var prevPage = pages[pages.length - 2]; // 上一级页面

    if (this.data.way == 2){
      prevPage.setData({
        user_id: e.currentTarget.dataset.user_id,
        user_name: e.currentTarget.dataset.user_name
      });
    } else if (this.data.way == 3){
      prevPage.setData({
        source_id: e.currentTarget.dataset.user_id,
        source_name: e.currentTarget.dataset.user_name,
        source_tel: e.currentTarget.dataset.phone,
      });
    } else if (this.data.way == 4) {
      prevPage.setData({
        user_id_m: e.currentTarget.dataset.user_id,
        user_name_m: e.currentTarget.dataset.user_name
      });
    }
    wx.navigateBack();
  },
})