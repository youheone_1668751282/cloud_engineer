// pages/subpackage/pages/crm/deviceEdit/deviceEdit.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    screenModeArray: [], //屏幕显示模式
    screenModeIndex: 0,
    filterList: [],
    filterArrey: [{
      filter_index: '',
      parts_name: '',
      residual_value: '',
      cycle: '',
    }],

    equipment_id: '', //设备id
    dataInfo: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      equipment_id: options.equipment_id
    })
    this.getConfigList(); //获取屏幕显示模式列表
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取设备详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'api/Equipmentlists/manageDevice',
      url_type: 2,
      data: {
        id: that.data.equipment_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var dataInfo = res.data.data;
          dataInfo.product_info.forEach((item, index) => {
            if (dataInfo.device_info.equipments_id == item.equipments_id){
              dataInfo.device_info.equipments_name = item.equipments_name;
            }
          })

          // 屏幕显示模式
          that.data.screenModeArray.forEach((item, index) => {
            if (dataInfo.device_info.screen_mode == item.id) {
              that.setData({
                screenModeIndex: index
              })
            }
          })

          // 滤芯模式
          var tempList = dataInfo.equipments_parts;
          dataInfo.parts_info.forEach((item, index) => {
            tempList.forEach((it_em, ind_ex) => {
              if (it_em.parts_id == item.parts_id) {
                it_em.filter_index = index;
                it_em.parts_id = item.parts_id;
                it_em.id = it_em.id;
              }
            })
          })

          that.setData({
            dataInfo: dataInfo,
            filterArrey: tempList,
            filterList: dataInfo.parts_info
          })
          console.log('得到的数组信息',that.data.filterList);
        }
      }
    });
  },

  //获取屏幕显示模式列表
  getConfigList() {
    var that = this;
    app.ajax({
      url: 'api/Customer/getSearchConfig',
      url_type: 2,
      data: {
        config_name: 'screen_mode'
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            screenModeArray: res.data.data
          })
        }
      }
    })
  },

  // 获取心跳
  getHeartbeat() {
    var that = this;
    app.ajax({
      url: 'House/Issue/heartbeat',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },
  // 下发套餐
  issuePackage() {
    var that = this;
    app.ajax({
      url: 'Common/Contract/issuePackage',
      // url_type: 2,
      data: {
        device_no: that.data.dataInfo.device_info.device_no
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },
  // 获取版本
  getVersion() {
    var that = this;
    app.ajax({
      url: 'House/Issue/version',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },
  // 最新状态
  syncRedisToDevice() {
    var that = this;
    app.ajax({
      url: 'House/Issue/syncRedisToDevice',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { 
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },

  //开关机
  machineOnOff(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/power_off',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        device_status: status==1?0:1
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.switch_machine']: status
            })
          }
        });
      }
    })
  },
  //屏幕状态开关
  screenOnOff(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/screen_status',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        screen_status: status
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.screen_status']: status
            })
          }
        });
      }
    })
  },
  //工作模式
  workModeSwitch(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/mode_switch',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        working_mode: status
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.working_mode']: status
            })
          }
        });
      }
    })
  },
  // 屏幕显示模式选择
  screenModeChange(e) {
    var that = this;
    var value = e.detail.value;
    var valArr = [];
    if(value == 3) {
      valArr = [1, 3];
    } else if(value == 2){
      valArr = [1, 2];
    } else if(value == 1) {
      valArr = [0, 3];
    } else{
      valArr = [0, 2];
    }
    app.ajax({
      url: 'House/Issue/switch_screen_display_mode',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        value: valArr
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              screenModeIndex: value
            })
          }
        });
      }
    })
  },

  // 设置套餐
  setMeal(e){
    var that = this;
    var detail = e.detail.value;
    app.ajax({
      url: 'House/Issue/data_sync',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        used_days: detail.used_days,
        remaining_days: detail.remaining_days,
        used_traffic: detail.used_traffic,
        remaining_traffic: detail.remaining_traffic
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  // 定时冲洗
  timedWashingInput(e) {
    this.setData({
      ['dataInfo.device_info.wash_regularly']: e.detail.value
    })
  },
  timedWashing() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modified_timed_washing',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        value: that.data.dataInfo.device_info.wash_regularly
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  // 强制冲洗
  flushingTimes(e) {
    this.setData({
      ['dataInfo.device_info.forced_flushing_time']: e.detail.value
    })
  },
  flushingTimes() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_mandatory_flushing_times',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        value: that.data.dataInfo.device_info.forced_flushing_time
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  // 进入检修状态时长
  maintenanceDurationInput(e) {
    this.setData({
      ['dataInfo.device_info.maintenance_duration']: e.detail.value
    })
  },
  maintenanceDuration() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_maintenance_parameters',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        value: that.data.dataInfo.device_info.maintenance_duration
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  // 心跳间隔
  heartbeatIntervalInput(e) {
    this.setData({
      ['dataInfo.device_info.heartbeat_interval']: e.detail.value
    })
  },
  heartbeatInterval() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_control_parameter_two',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        key: 1,
        value: that.data.dataInfo.device_info.heartbeat_interval
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  // 断网重连时长
  brokenNetworkInput(e) {
    this.setData({
      ['dataInfo.device_info.broken_network_reconnection']: e.detail.value
    })
  },
  brokenNetwork() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_control_parameter_two',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        key: 2,
        value: that.data.dataInfo.device_info.broken_network_reconnection
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
      }
    })
  },

  //测试模式开关
  testOnOff(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/switch_test_mode',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        value: status
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.debug_mode']: status
            })
          }
        });
      }
    })
  },
  //纯水TDS检测开关
  pureWaterInput(e) {
    this.setData({
      ['dataInfo.device_info.pure_water_default']: e.detail.value
    })
  },
  pureWaterOnOff(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/modify_control_parameter_one',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        key: 1,
        switch: String(status),
        value: that.data.dataInfo.device_info.pure_water_default
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.pure_water_TDS_detection_switch']: status
            })
          }
        });
      }
    })
  },
  //原水TDS检测开关
  rawWaterInput(e) {
    this.setData({
      ['dataInfo.device_info.raw_water_default']: e.detail.value
    })
  },
  rawWaterOnOff(e) {
    var that = this;
    var status = e.currentTarget.dataset.state;
    app.ajax({
      url: 'House/Issue/modify_control_parameter_one',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
        key: 2,
        switch: String(status),
        value: that.data.dataInfo.device_info.pure_water_default
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.setData({
              ['dataInfo.device_info.raw_water_TDS_detection_switch']: status
            })
          }
        });
      }
    })
  },

  // 滤芯同步数据
  syncFilterData(){
    var that = this;
    app.ajax({
      url: 'Common/Device/syncFilterData',
      // url_type: 2,
      data: {
        equipment_id: that.data.equipment_id
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },

  // 添加滤芯模式
  addFilter(e) {
    let that = this;
    let filterArrey = that.data.filterArrey;
    if (filterArrey.length >= 5) {
      app.showToast('最多五级滤芯');
      return
    }
    filterArrey.push({
      filter_index: '',
      parts_name: '',
      residual_value: '',
      cycle: '',
    })
    that.setData({
      filterArrey: filterArrey
    })
  },
  // 删除滤芯模式
  reduceFilter(e) {
    let that = this;
    if (that.data.filterArrey.length <= 1) {
      return
    }
    let index1 = e.currentTarget.dataset.index;
    let filterArrey = that.data.filterArrey;
    if (that.data.filterArrey[index1].id){
      app.ajax({
        url: 'User/Parts/delEquipmentsParts',
        // url_type: 2,
        data: {
          equipment_id: that.data.equipment_id,
          equipments_parts_id: filterArrey[index1].id,
        },
        success: function (res) {
          app.showToast(res.data.message, "none", 2000, function () {
            if (res.data.code == 1000) {
              filterArrey.splice(index1, 1);
              that.setData({
                filterArrey: filterArrey
              })
            }
          });
        }
      })
    } else {
      filterArrey.splice(index1, 1);
      that.setData({
        filterArrey: filterArrey
      })
    }
  },
  // 滤芯模式选择
  filterModeChange(e){
    var index = e.currentTarget.dataset.index;
    var temp = this.data.filterArrey;
    var value = e.detail.value;
    temp[index].filter_index = value;
    temp[index].parts_id = this.data.filterList[value].parts_id;
    temp[index].id = this.data.filterList[value].id;
    this.setData({
      filterArrey: temp
    })
  },
  // 剩余值
  residualInput(e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      ['filterArrey[' + index + '].residual_value']: e.detail.value
    })
  },
  // 最大值
  cycleInput(e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      ['filterArrey[' + index + '].cycle']: e.detail.value
    })
  },
  // 确定滤芯模式
  sureFilter(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    app.ajax({
      url: 'User/Parts/editEquipmentsParts',
      // url_type: 2,
      data: {
        equipment_id: that.data.equipment_id,
        sn: that.data.dataInfo.device_info.device_no,
        parts_id: that.data.filterArrey[index].parts_id,
        equipments_parts_id: that.data.filterArrey[index].id,
        residual_value: that.data.filterArrey[index].residual_value,
        value: that.data.filterArrey[index].residual_value,
        cycle: that.data.filterArrey[index].cycle,
        key: Number(index) + 1, //滤芯等级
        type: 2,
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },

  // 重启设备
  rebootDevice(){
    var that = this;
    app.ajax({
      url: 'House/Issue/remote_restart',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },
  // 恢复出厂设置
  factoryReset(){
    var that = this;
    app.ajax({
      url: 'House/Issue/factory_data_reset',
      // url_type: 2,
      data: {
        sn: that.data.dataInfo.device_info.device_no,
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () {
          if (res.data.code == 1000) {
            that.getInfo();
          }
        });
      }
    })
  },
})