 // pages/subpackage//pages/crm/workOrderManag/workOrderManag.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSeachIcon: true,  //搜索_____________框搜索图标
    showCondition: false, //搜索_____________展示更多的搜索条件
    keyword: "",        //搜索_____________搜索桩号 
    startTime: "",        //搜索_____________开始时间
    endTime: "",          //搜索_____________结束时间
    typeList: [{ name: '全部', id: '' }],                  //搜索_____________回访状态
    typeChose: 0,//回访状态  
   //审核状态
    typeChoseList: [{ name: '请选择处理状态', id: '' }, { name: '无效工单', id: 2 }, { name: '问题已处理', id: 3 }, { name: '审核通过,待派单', id: 5 }],    choseIndex: 0, 
    //更换状态
    typeStateList: [{ name: '请选择处理状态', id: '' }, { name: '无效工单', id: 2 }, { name: '问题已处理', id: 3 }],    
    stateIndex:0,
    //回访||再回访状态
    typeReVisitList: [{ name: '请选择处理状态', id: '' }, { name: '已回访', id: 2 }, { name: '待再次回访', id: 3 }],
    reVisitIndex: 0,
    visitrecordList:[],//以往备注

    orderType: [{ id: '', name: '请选择工单状态' }],
    otychose: 0,//工单状态
    checkList: [{ name: '全部', value: '1' }],
    checkchose: 0,//业务类型
    areaList: [{ company_name: '请选择区域', o_id: '' }],
    tyepIndex: 0,//所属区域
    adminList: [{ company_name: '请选择行政中心', a_id: '' }],
    adminIndex:0,//所属行政中心
    workOrderList:[],//工单列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading: false,
    client: wx.getStorageSync('client'),
    remarks:'',
    popShow:false,//是否展示弹窗
    work_order_id: '',//当前弹窗选中的工单ID
    popType:1,//弹窗类型 1 审核  2更换状态 3回访  4再回访  5备注
    visit_status:'',
    work_order_status:'',
    op_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getConfigList(1);//回访状态
    this.getConfigList(2);//工单状态
    //this.getConfigList(3);//工单审核
    this.getBelongToAreas();//工单状态配置
    this.getBusinessType();//获取工单业务类型
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    pagea = 1;
    this.setData({
      workOrderList: [],
      is_load: false,
      hasMore: true,
    })
    this.getWorkOrder();//获取列表
  },
  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    // this.setData({
    //   startTime: "",
    //   endTime: ''
    // })
    pagea = 1;
    this.setData({
      workOrderList: [],
      is_load: false,
      hasMore: true,
    })
    this.getWorkOrder();//获取列表
  },
  //搜索_____________搜索框选中
  searchFocus() {
    this.setData({
      showSeachIcon: false,
    })
  },
  //搜索_____________搜索输入框清除图标
  inputClear() {
    this.setData({
      keyword: '',
      showSeachIcon: true
    })
  },
  //搜索_____________搜索失去焦点
  searchBlur(e) {
    const result = e.detail.value;
    if (result === "") {
      this.setData({
        showSeachIcon: true,
      })
    }
  },
  //搜索_____________点击筛选
  tapFilter() {
    let showCondition = this.data.showCondition;
    this.setData({
      showCondition: !showCondition,
    })
  },
  //搜索_____________隐藏筛选条件
  hideFilter() {
    this.setData({
      showCondition: false,
    })
  },
  //搜索_____________重置搜索条件
  searchReset() {
    this.setData({
      startTime: "",
      endTime: '',
      keyword: "",
      typeChose: 0,
      otychose: 0,
      checkchose: 0,
      tyepIndex:0,
      adminIndex:0,
      adminList: [{ company_name: '请选择行政中心', a_id: '' }],
    })
    
  },
  //搜索_____________搜索确定
  searchConfirm() {
    this.setData({
      showCondition: false
    })
    pagea = 1;
    this.setData({
      workOrderList: [],
      is_load: false,
      hasMore: true,
    })
    this.getWorkOrder();//获取列表
  },
  //搜索_____________开始时间
  startDateChange: function (e) {
    this.setData({
      startTime: e.detail.value
    })
  },
  //搜索_____________开始时间
  endDateChange: function (e) {
    this.setData({
      endTime: e.detail.value
    })
  },
  //搜索_____________选择类型
  choseType: function (e) {
    this.setData({
      typeChose: e.currentTarget.dataset.chose
    })
  },
  //搜索_____________工单状态
  bindChoseOrderType: function (e) {
    // console.log('e.detail.value', e)
    this.setData({
      otychose: e.detail.value
    })
  },
  //搜索_____________对账状态
  choseCheck: function (e) {
    this.setData({
      checkchose: e.currentTarget.dataset.chose
    })
  },
  //搜索_____________所属区域
  bindPickerChange: function (e) {
    var op_id = this.data.areaList[e.detail.value].o_id;
    if (this.data.tyepIndex == e.detail.value){return false};
    this.setData({
      tyepIndex: e.detail.value,
      op_id: op_id
    })
    if (op_id){
      this.getAdministrative(op_id);
    }
   
  },
  //搜索____________行政中心
  adminPickerChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      adminIndex: e.detail.value
    })
  },
  // 审核
  trialFun: function () {
    wx.showModal({
      title: '是否指派工程部？',
      content: '',
      cancelText:'否',
      cancelColor:'#999999',
      confirmText:'是',
      confirmColor:'#6CA9FF',
      success(res) {
        if (res.confirm) {
          console.log('确认指派')
        } else if (res.cancel) {
          console.log('取消指派')
        }
      }
    })
  },
  //添加工单
  navAddworkOrder(){
    wx.navigateTo({
      url: '../addworkOrder/addworkOrder',
    })
  },
  //查看工单详情
  navWorkOrderInfo(){
    return false;
    wx.navigateTo({
      url: '../workOrderInfo/workOrderInfo',
    })
  },
  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getWorkOrder(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      workOrderList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getWorkOrder(); // 获取订单列表
  },
  // 获取订单列表
  getWorkOrder: function () {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/Workorder/getLists',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        keywords: that.data.keyword,
        workorderstatus: that.data.orderType[that.data.otychose].id,
        visit_status: that.data.typeChose==0?'':that.data.typeChose,
        status: that.data.checkchose == 0 ? '':that.data.checkchose,
        a_id: that.data.adminList[that.data.adminIndex].a_id,
        o_id: that.data.op_id,
        startTime: this.data.startTime,
        endTime: this.data.endTime
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.workOrderList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          console.log('最后的数据', newlist)
          that.setData({
            workOrderList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false,
          popShow: false,
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },
  //获取搜索配置列表
  getConfigList(type) {
    var that = this;
    var config = '';
    if (type == 1) { //回访状态
      config = 'work_order_visit_status';
    } else if (type == 2) { //工单状态
      config = 'work_order_status';
    } else if (type == 3){
      config = 'audit_status';
    }
    app.ajax({
      url: 'api/Customer/getSearchConfig',
      url_type: 2,
      data: {
        config_name: config
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (type == 1) { //回访状态
            that.setData({
              typeList: res.data.data
            })
          } else if (type == 2) { //工单状态
            var orderType = that.data.orderType;
            orderType = orderType.concat(res.data.data);
            that.setData({
              orderType
            })
          } else if (type == 3) { //工单审核
            var typeChoseList = that.data.typeChoseList;
            typeChoseList = orderType.concat(res.data.data);
            that.setData({
               typeChoseList
            })
          } 
        }else{
          app.showToast('获取工单配置失败');
        }
      }
    })
  },
  //获取所属区域;
  getBelongToAreas() {
    var that = this;
    app.ajax({
      url: 'api/Operation/getList',
      url_type: 2,
      data: {
        pageSize:-1
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var areaList=that.data.areaList;
          areaList = areaList.concat(res.data.data);
          that.setData({
            areaList
          })
        } else { 
          app.showToast('获取区域配置失败');
        }
      }
    })
  },
  //获取所属行政中心
  getAdministrative(op_id) {
    var that = this;
    app.ajax({
      url: 'api/Administrative/getAdministrative',
      url_type: 2,
      data: {
        operation_id: op_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var adminList = that.data.adminList;
          adminList = adminList.concat(res.data.data);
          that.setData({
            adminList
          })
        } else {
          adminList=[];
          that.setData({
            adminList
          })
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //获取工单业务类型
  getBusinessType() {
    var that = this;
    app.ajax({
      url: 'api/Workorder/getBusinessType',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            checkList:res.data.data
          })
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //审核状态列表
  bindChoseIndexFun: function (e) {
    this.setData({
      choseIndex: e.detail.value
    })
  }, 
  //更换状态列表
  bindStatesIndexFun: function (e) {
    this.setData({
      stateIndex: e.detail.value
    })
  }, 
  //回访 || 再回访
  bindreVisitIndexFun: function (e) {
    this.setData({
      reVisitIndex: e.detail.value
    })
  }, 
  //备注输入
  remarkInput:function(e){
    this.setData({
      remarks: e.detail.value
    })
  },
  //点击审核
  examineFun:function(e){   
    this.setData({
      choseIndex:0,
      popType:1,
      popShow:true,
      work_order_id: e.currentTarget.dataset.id
    })
  },
  //更换状态
  chengeType:function(e){
    this.setData({
      stateIndex:0,
      popType: 2,
      popShow: true,
      work_order_id: e.currentTarget.dataset.id,
      remarks:''
    })
  },
  //请选择 type3回访 4再回访
  backType:function(e){
    var type = e.currentTarget.dataset.type;
    var id = e.currentTarget.dataset.id;
    this.setData({
      visitrecordList:[],
      reVisitIndex:0,
      popType: type,
      popShow: true,
      work_order_id: id,
      remarks:''
    })
    if (type == 4) {
      // this.getvisitrecord(id);暂时不显示
    }
  },
  //备注 type==5
  remarkFun(e){
    var id = e.currentTarget.dataset.id;
    //this.getRemarksFun(id); 暂时不显示
    this.setData({
      popType: 5,
      popShow: true,
      work_order_id: id,
      remarks: ''
    })
  },

  //取消
  cancelFun:function(){
    this.setData({
      popShow: false
    })
  },
  //确定 审核  popType:1 审核  2更换状态 3回访  4再回访  都统一处理到一个里面  
  confirmFunA:function(){
    var that=this;
    var popType = that.data.popType;
    var remarks = that.data.remarks;
    var work_order_id = that.data.work_order_id;
    var remarks = that.data.remarks;
    //popType==1审核 popType=2 更换状态
    var work_order_status = popType == 1 ? that.data.typeChoseList[that.data.choseIndex].id : that.data.typeStateList[that.data.stateIndex].id; 
    //popType==3 || popType=4 都是一个picker
    var visit_status = that.data.typeReVisitList[that.data.reVisitIndex].id;
    console.log('remarks>>', remarks, work_order_status, work_order_id);
    var  referData={};
    var  referUrl='';
    if (popType == 1 || popType==2){
      if (work_order_status == '') {
        app.showToast('请选择工单状态');
        return false;
      }
      referUrl ='api/Workorder/examine';
      referData={
        work_order_id: work_order_id,
        work_order_status: work_order_status,
        remarks: remarks
      }
    } else if (popType == 3 || popType == 4){
      if (visit_status == '') {
        app.showToast('请选择状态');
        return false;
      }
      referUrl ='api/workorder/returnvisit';
      referData = {
        work_order_id: work_order_id,
        visit_status: visit_status,
        remarks: remarks
      }
    } else if (popType==5){
      if (remarks == '') {
        app.showToast('请输入备注');
        return false;
      }
      referUrl = 'api/workorder/addremarks';
      referData = {
        work_order_id: work_order_id,
        remarks: remarks
      }
    }
    
    app.ajax({
      url: referUrl,
      url_type: 2,
      data: referData,
      success: function (res) {
        if (res.data.code == 1000) {
         app.showToast(res.data.msg);
          pagea = 1;
          that.setData({
            workOrderList: [],
            is_load: false,
            hasMore: true,
          })
          that.getWorkOrder();//获取列表
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //指派
  assign(e){
    wx.navigateTo({
      url: '../dispatch/dispatch?work_order_id=' + e.currentTarget.dataset.id + '&change=' + e.currentTarget.dataset.change,
    })
  },
  //编辑工单
  editOrder(e){
    wx.navigateTo({
      url: '../editWorkerOrder/editWorkerOrder?work_order_id=' + e.currentTarget.dataset.id,
    })
  },
  //回退工单
  backOreder(e){
    var that = this;
    var work_order_id = e.currentTarget.dataset.id;
    var contract_id = e.currentTarget.dataset.contract_id;
    wx.showModal({
      title: '是否确认要回退吗？',
      content: '将修改对应合同,设备,订单,工单状态？',
      cancelText: '否',
      cancelColor: '#999999',
      confirmText: '是',
      confirmColor: '#6CA9FF',
      success(res) {
        if (res.confirm) {
          console.log('确认回退');
          that.fallback(work_order_id, contract_id);
        } else if (res.cancel) {
          console.log('取消回退');
        }
      }
    })
  },
  //回退
  fallback(work_order_id, contract_id){
    var that = this;
    app.ajax({
      url: 'api/Contract/cancel',
      url_type: 2,
      data: {
        work_order_id: work_order_id,
        contract_id: contract_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.msg);
          pagea = 1;
          that.setData({
            workOrderList: [],
            is_load: false,
            hasMore: true,
          })
          that.getWorkOrder();//获取列表
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //更新关系 
  updateRelation(e){
    var that = this;
    var contract_no = e.currentTarget.dataset.contract_no;
    var work_order_id = e.currentTarget.dataset.id;
    app.ajax({
      url: 'User/Relation/levelSettlement',
      //url_type: 2,
      data: {
        contract_no: contract_no
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message);
          pagea = 1;
          that.setData({
            workOrderList: [],
            is_load: false,
            hasMore: true,
          })
          that.getWorkOrder();//获取列表
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //获取以往备注(回访备注) 
  getvisitrecord(work_order_id) {
    var that = this;
    app.ajax({
      url: 'api/workorder/getvisitrecord',
      url_type: 2,
      data: {
        work_order_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            visitrecordList: res.data.data,
          })
        } else {
        }
      }
    })
  },
  //获取 备注的备注()
  getRemarksFun(work_order_id) {
    var that = this;
    app.ajax({
      url: 'api/Workorder/getRemarks',
      url_type: 2,
      data: {
        work_order_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            remarksList: res.data.data,
          })
        } else {
        }
      }
    })
  },

  
})