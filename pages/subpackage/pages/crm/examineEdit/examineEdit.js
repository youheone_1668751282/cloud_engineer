// pages/subpackage//pages/crm/examineEdit/examineEdit.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    auth_id: '',//认证id

    typeList: [], //审核状态
    tyepIndex:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      auth_id: options.id
    })
    this.getConfigList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  //获取审核状态配置列表
  getConfigList() {
    var that = this;
    app.ajax({
      url: 'api/Customer/getSearchConfig',
      url_type: 2,
      data: {
        config_name: 'realname_auth_status'
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var temp = [];
          res.data.data.forEach((item)=>{
            if(item.id>0){
              temp.push(item);
            }
          })
          that.setData({
            typeList: temp
          })
        }
      }
    })
  },

  // 选审核状态
  bindPickerChange: function (e) {
    this.setData({
      tyepIndex: e.detail.value
    })
  },
  //提交的表单数据
  formSubmit(e) {
    var that = this;
    app.ajax({
      url: 'api/Realname/check',
      url_type: 2,
      data: {
        auth_id: that.data.auth_id,
        status: that.data.typeList[that.data.tyepIndex].id,
        remarks: e.detail.value.remark
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function() {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //取消
  cancel(){
    wx.navigateBack();
  },
})