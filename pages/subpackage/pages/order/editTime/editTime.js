// pages/order/editTime/editTime.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    go_right: '/images/go_right.png',
    id: '', //日程ID
    work_order_id: '',//工单ID
    date: '',
    weight_list: [  //等级管理
      { id: 1, name: '一般' },
      { id: 2, name: '重要' },
      { id: 3, name: '紧急' },
    ],
    schedule_weight: '',//等级值
    startTime: '',
    endTime: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id || '';
    var work_order_id = options.work_order_id;
    var schedule_weight = '';
    var date = options.date||'';
    if (options.schedule_weight){
      schedule_weight = Number(options.schedule_weight) - 1;
    }
    var timestamp = parseFloat(new Date(date).getTime()+ 7 * 24 * 60 * 60*1000);
    var time = new Date(timestamp);
    var y = time.getFullYear();  //获取当前年份
    var m = (time.getMonth() + 1).toString().padStart(2, '0');
    //获取当前月份   padStart()方法 es6新属性 不满两位数时在前面加零
    var d = (time.getDate()).toString().padStart(2, '0');  //获取当前日期
    var endTime = y + "-" + m + "-" + d;
    console.log(endTime);//返回内容格式为2019-04-01


    this.setData({
      id: id,
      work_order_id: work_order_id,
      schedule_weight: schedule_weight,
      date: date,
      startTime: date,
      endTime: endTime
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //选择日期
  bindDateChange: function (e) {
    let that = this;
    let date = e.detail.value;
    that.setData({
      date: date
    })
  },
  //选择等级
  bindChange(e){
    let that = this;
    let schedule_weight = e.detail.value;
    that.setData({
      schedule_weight: schedule_weight,
    })
  },
  // 确认预约时间
  sureTime: function (e) {
    let that = this;
    console.log(e.detail.formId);
//    app.saveFormId(e.detail.formId);
    if (that.data.date != '') {
      wx.showModal({
        title: '提示',
        content: '是否确认日程时间为：\r\n' + that.data.date,
        success(res) {
          if (res.confirm) {
            app.ajax({
              // 修改日程
              url: 'Engineer/Schedule/arrangeSchedule',
              data: {
                id: that.data.id, 
                target_id: that.data.work_order_id,
                schedule_time: that.data.date,
                schedule_weight: Number(that.data.schedule_weight) + 1 || ''
              },
              success: function (res) {
                if (res.data.code == 1000) {
                  app.showToast("确认预约成功", "none", 2000, function () { });
                  wx.redirectTo({
                    url: '/pages/subpackage/pages/date/scheduleManage/scheduleManage'
                  });

                } else {
                  app.showToast(res.data.message, "none", 2000, function () { });
                }



              }
            })
          }
          if (res.cancel) {

          }
        }
      })


    } else {
      app.showToast('请选择时间', 'none', 2000, function () { })
    }
  }
})