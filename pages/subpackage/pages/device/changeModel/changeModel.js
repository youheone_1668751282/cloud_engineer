// pages/subpackage/pages/device/changeModel/changeModel.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeList: [], //机型
    tyepIndex: 0,
    machine_id:"",
    equipment_id:"",
    contract_id:"",

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      equipment_id: options.equipment_id,
      contract_id: options.contract_id
    })
    console.log("equipment_id",options.equipment_id)
    this.getConfigList();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
/**
   * 设备详情
   */
  equipmentDetail:function(){
    var that = this;
    var equipment_id = that.data.equipment_id;
    app.ajax({
      url:"Engineer/Equipmentlists/equipmentDetail",
      data: { "equipment_id": equipment_id},
      success:function(res){
          if(res.data.code==1000){
            that.setData({
              machine_id: res.data.data.machine_id,
            })
            that.data.typeList.forEach((item,index) => {
              if (item.machine_id ==res.data.data.machine_id ) {
                that.setData({
                  tyepIndex:index
                })
              }
            })
          }
      }
    })
  },
  //获取机型列表
  getConfigList() {
    var that = this;
    app.ajax({
      url: 'Engineer/Equipmentlists/getMachineList',
      data: {
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var temp = [];
          res.data.data.forEach((item) => {
            if (item.machine_id > 0) {
              temp.push(item);
            }
          })
          that.setData({
            typeList: temp
          })
          that.equipmentDetail();
        }
      }
    })
  },

  // 选审核状态
  bindPickerChange: function (e) {
    this.setData({
      tyepIndex: e.detail.value
    })
  },
  //提交的表单数据
  formSubmit(e) {
    var that = this;
    app.ajax({
      url: 'Engineer/Equipmentlists/change_model',
      data: {
        equipment_id: that.data.equipment_id,
        contract_id: that.data.contract_id,
        device_model: that.data.typeList[that.data.tyepIndex].machine_id,
        //remarks: e.detail.value.remark
      },
      success: function (res) {
        app.showToast(res.data.message, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //取消
  cancel() {
    wx.navigateBack();
  },
})
