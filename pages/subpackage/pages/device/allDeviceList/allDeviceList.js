// pages/subpackage/pages/device/deviceManage/deviceManage.js

var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSeachIcon: true,  //搜索_____________框搜索图标
    showCondition: false, //搜索_____________展示更多的搜索条件
    keyword: "",          //搜索_____________搜索关键字内容
    deviceTypeIndex: -1,   //搜索_____________设备类型下标
    deviceType: [],
    onlineStatusIndex: -1, //搜索_____________在线状态下标
    onlineStatus: [],
    deviceStatusIndex: -1, //搜索_____________设备状态下标
    deviceStatus: [],

    dataList: [],//设备列表
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
  },

  /*** 生命周期函数--监听页面加载*/
  onLoad: function (options) {
    var that = this;
    that.setData({
      work_order_id: options.work_order_id
    });
    this.getConfigList(1); //设备类型
    this.getConfigList(2); //设备在线离线状态
    this.getConfigList(3); //设备状态
  },
  /*** 生命周期函数--监听页面显示*/
  onShow: function () {
    var that = this;
    load = true;
    _PAGE = 1;
    that.setData({
      deviceTypeIndex: -1,
      onlineStatusIndex: -1,
      deviceStatusIndex: -1,
      dataList: [],
    });
    console.log("1111")
    that.getList();
  },
  //确认选择，返回上一页
  chooseSure(e) {
    var item = e.currentTarget.dataset.item;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1]; // 当前页面
    var prevPage = pages[pages.length - 2]; // 上一级页面

    prevPage.setData({
      device_old: item.equipment_id,//选择的设备id
      device_no: item.device_no,//选择的设备编号
      user_name: item.realname,//选择的设备用户名称
      user_tel: item.telphone,//选择的设备用户电话
    })

    wx.navigateBack();
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    _PAGE = 1
    that.getList();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getList();
  },

  //获取搜索配置列表
  getConfigList(type) {
    var that = this;
    var config = '';
    if (type == 1) { //设备类型
      config = 'equipment_type';
    } else if (type == 2) { //设备在线离线状态
      config = 'equipmentlist_status';
    } else if (type == 3) { //设备状态
      config = 'device_status';
    }
    app.ajax({
      url: 'Common/Common/getSearchConfig',
      data: {
        config_name: config
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (type == 1) { //设备类型
            that.setData({
              deviceType: res.data.data
            })
          } else if (type == 2) { //设备在线离线状态
            that.setData({
              onlineStatus: res.data.data
            })
          } else if (type == 3) { //设备状态
            that.setData({
              deviceStatus: res.data.data
            })
          }
        }
      }
    })
  },

  //获取设备列表
  getList() {
    var that = this;
    var work_order_id = that.data.work_order_id;
    app.ajax({
      url: 'Engineer/Equipmentlists/getEquipmentlists',
      data: {
        page: _PAGE,
        pageSize: _PAGESIZE,
        keywords: that.data.keyword,
        equipment_type: that.data.deviceTypeIndex == -1 ? '' :that.data.deviceType[that.data.deviceTypeIndex].id,
        statusname: that.data.onlineStatusIndex == -1 ? '' :that.data.onlineStatus[that.data.onlineStatusIndex].id,
        device_status: that.data.deviceStatusIndex == -1 ? '' :that.data.deviceStatus[that.data.deviceStatusIndex].id,
        work_order_id:work_order_id,
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              dataList: res.data.data.data,
              empty: false,
            })
          } else {
            that.setData({
              dataList: that.data.dataList.concat(res.data.data.data),
              empty: false
            });
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.page.total_page <= _PAGE) {
            hasMore = false;
          }
          load = false;
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    });
  },

  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    this.setData({
      deviceTypeIndex: -1,
      onlineStatusIndex: -1,
      deviceStatusIndex: -1,
    })
    _PAGE = 1;
    this.getList();
  },
  //搜索_____________搜索框选中
  searchFocus() {
    this.setData({
      showSeachIcon: false,
    })
  },
  //搜索_____________搜索输入框清除图标
  inputClear() {
    this.setData({
      keyword: '',
      showSeachIcon: true
    })
    this.getList();
  },
  //搜索_____________搜索失去焦点
  searchBlur(e) {
    const result = e.detail.value;
    if (result === "") {
      this.setData({
        showSeachIcon: true,
      })
    }
  },
  //搜索_____________点击筛选
  tapFilter() {
    let showCondition = this.data.showCondition;
    this.setData({
      showCondition: !showCondition,
    })
  },
  //搜索_____________隐藏筛选条件
  hideFilter() {
    this.setData({
      showCondition: false,
    })
  },
  //搜索_____________重置搜索条件
  searchReset() {
    this.setData({
      keyword: "",
      deviceTypeIndex: -1,
      onlineStatusIndex: -1,
      deviceStatusIndex: -1,
    })
    _PAGE = 1;
    this.getList();
  },
  //搜索_____________搜索确定
  searchConfirm() {
    this.setData({
      showCondition: false
    })
    _PAGE = 1;
    this.getList();
  },
  //搜索_____________筛选状态类型
  checkType(e) {
    var type = e.currentTarget.dataset.type;
    var index = e.currentTarget.dataset.index;
    if (type == 1) { //设备类型
      this.setData({
        deviceTypeIndex: index
      })
    }
    if (type == 2) { //在线状态
      this.setData({
        onlineStatusIndex: index
      })
    }
    if (type == 3) { //设备状态
      this.setData({
        deviceStatusIndex: index
      })
    }
  },
})