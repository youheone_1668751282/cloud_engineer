var util = require("../../utils/util.js");
var app = getApp();
var is_change= false;
let areaInfo_name = [],area_id_Info = []; //选中地区名称 选中的地区id信息
Component({
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    valueSet:{
      type:Object,
      observer: 'changeValueSet'
    },
    // 回显的地址名称数组
    valueName:{
      type: Object,
      observer: 'changeValueName'
    }
  },
  data: {
    // 这里是一些组件内部数据
    ak_code: '3VPBZ-KJ5LQ-7Y55U-GZ6HH-BJ2XH-RBB6B',
    addshow:false,
    province_s: '',
    city_s: '',
    area_s:'',
    value_sel: [0, 0, 0], //对应数组下标
    province_s: [],
    city_s: [],
    area_s: [],
    animationData: {}, //动画设置
    animationAddressMenu: {},
    
  },
  lifetimes:{
    //初次创建的时候
    created: function () {
      var that=this;
      that.greadMove();//执行动画
      that.getProvince(); //获取省份信息     
      
    },
  },
  pageLifetimes: {
   
    show: function () {
      // 页面被展示
      
    },
    hide: function () {
      // 页面被隐藏
    },
    resize: function (size) {
      // 页面尺寸变化
    }
  },
  //设置默认值
  attached: function(){
    var that=this;
    var value_Set = that.properties.valueSet||[];
    // console.log("设置默认地址", this.properties.valueSet);
    if (value_Set.length>0){
      that.getCityLocation(value_Set[0], value_Set[1]);
    }else{
      var value_Name = that.properties.valueName||[];
      // console.log('value_Name', value_Name);
      if (value_Name.length > 0) {
        that.getCityCode(value_Name[0], value_Name[1], value_Name[2]);
      }
    }
    

  },
  methods: {
    //创建初始动画
    greadMove() {
      var that=this;
      var animation = wx.createAnimation({
        duration: 500,
        transformOrigin: "50% 50%",
        timingFunction: 'ease',
      })
      that.animation = animation;
    },
    // 处理省市县联动逻辑
    cityChange: function (e) {
      var that = this;
      var value_sel = e.detail.value; //获取选中的id数组
      var provinces = this.data.provinces
      var citys = this.data.citys
      var areas = this.data.areas
      var province_Num = value_sel[0]
      var city_Num = value_sel[1]
      var county_Num = value_sel[2]
      //判断是否需要切换市区级
      if (this.data.value_sel[0] != province_Num) {
        var provinceid = that.data.province_s[province_Num].id; //获取省id查询市级信息
        that.getCity(provinceid); //获取市级信息
        this.setData({
          value_sel: [province_Num, 0, 0],
        })
        is_change= ''; //置空详细地址
      } else if (this.data.value_sel[1] != city_Num) { //判断是否需要切换区域
        var cityid = that.data.city_s[city_Num].id;
        that.getArea(cityid); //获取区域信息
        this.setData({
          value_sel: [province_Num, city_Num, 0]
        })
        is_change = ''; //置空详细地址
      } else {
        this.setData({
          value_sel: [province_Num, city_Num, county_Num]
        })
        is_change = ''; //置空详细地址
      }
    },
    //获取省级信息 getProvince
    getProvince: function () {
      var that = this;
      app.ajax({
        url: 'Common/Areas/areas',
        method: "POST",
        data: {
          parent_id: 0
        },
        success: function (res) {
          if (res.data.code == 1000) {
            that.setData({
              province_s: res.data.data
            })
            //初次进入才会调用 获取市区级
            if (that.data.city_s.length == 0) {
              that.getCity(res.data.data[0].id);
            }
          } else {
            app.showToast(res.data.message, "none", 2000, function () { });
          }
        }
      })
    },
    //获取市信息 getCity
    getCity: function (provinceid) {
      var that = this;
      app.ajax({
        url: 'Common/Areas/areas',
        method: "POST",
        data: {
          parent_id: provinceid
        },
        success: function (res) {
          if (res.data.code == 1000) {
            that.setData({
              city_s: res.data.data
            });
            //重置区域信息 
            that.getArea(res.data.data[0].id);
          } else {
            app.showToast(res.data.message, "none", 2000, function () { });
          }
        }
      })
    },
    //获取区域信息 getArea
    getArea: function (cityid) {
      var that = this;
      app.ajax({
        url: 'Common/Areas/areas',
        method: "POST",
        data: {
          parent_id: cityid
        },
        success: function (res) {
          if (res.data.code == 1000) {
            that.setData({
              area_s: res.data.data
            })
          } else {
            app.showToast(res.data.message, "none", 2000, function () { });
          }
        }
      })
    },
    //点击黑色区域关闭地址选择
    hideCitySelected: function (e) {
      this.startAddressAnimation(false)
    },
    //取消事件
    _cityCancel(e) {
      this.startAddressAnimation(false);
      // this.triggerEvent('city_cancel', e); //暂时不往外放
    },
    //确定事件
    citySure() {
      var that = this
      var value_sel = that.data.value_sel;
      that.startAddressAnimation(false)
      // 将选择的城市信息放入数组后期方便操作
      areaInfo_name = [];
      areaInfo_name.push(that.data.province_s[value_sel[0]].area_name);
      areaInfo_name.push(that.data.city_s[value_sel[1]].area_name);
      areaInfo_name.push(that.data.area_s[value_sel[2]].area_name);
      area_id_Info = []; //区域id
      area_id_Info.push(that.data.province_s[value_sel[0]].id);
      area_id_Info.push(that.data.city_s[value_sel[1]].id);
      area_id_Info.push(that.data.area_s[value_sel[2]].id);
      this.triggerEvent('sureFun', { areaName: areaInfo_name, areaId: area_id_Info}); 
    },
    // 执行动画
    startAddressAnimation: function (isShow) {
      var that = this
      if (isShow) {
        that.animation.translateY(0 + 'vh').step()
      } else {
        that.animation.translateY(60 + 'vh').step()
      }
      that.setData({
        animationAddressMenu: that.animation.export(),
        addshow: isShow,
      })
    },
    //根据经纬度获取城市信息
    getCityLocation: function (latitude, longitude) {
      var that = this;
      console.log('地址', latitude, longitude);
      app.ajax({
        // url: 'https://api.map.baidu.com/geocoder/v2/?ak=' + that.data.ak_code + '&location=' + latitude + ',' + longitude + '&output=json',
        url: 'https://apis.map.qq.com/ws/geocoder/v1/?location=' + latitude + ',' + longitude + '&key=' + that.data.ak_code + '&get_poi=1',
        method: 'GET',
        data: {},
        header: {
          'Content-Type': 'application/json'
        },
        success: function (res) {
          // success  
          //百度地图返回参数
          // that.getCityCode(res.data.result.addressComponent.province, res.data.result.addressComponent.city, res.data.result.addressComponent.district)
          //腾讯地图返回参数
          //console.log('地址', res.data);
          that.getCityCode(res.data.result.address_component.province, res.data.result.address_component.city, res.data.result.address_component.district)
          // 更新用户地址
          // that.updateAddress(res.data.result.addressComponent);
        },
        fail: function () { },

      })
    },
    //根据地址查询地区code
    getCityCode: function (province, city, district) {
      var that = this;
      console.log('11111111');
      app.ajax({
        url: 'Common/Areas/getAreasCode',
        method: "POST",
        data: {
          province: province,
          city: city,
          area: district
        },
        success: function (res) {
          //console.log(res.data)
          if (res.data.code == 1000) {
            areaInfo_name = []; //区域名称
            areaInfo_name.push(res.data.data.province_info.area_name);
            areaInfo_name.push(res.data.data.city_info.area_name);
            areaInfo_name.push(res.data.data.area_info.area_name);
            area_id_Info = []; //区域id
            area_id_Info.push(res.data.data.province_info.id);
            area_id_Info.push(res.data.data.city_info.id);
            area_id_Info.push(res.data.data.area_info.id);
            // that.setData({
            //   codeInfo: res.data.data
            // });

            that.setProDefault(res.data.data);
          } else {
            // app.showToast(res.data.message);
          }
        }
      })
    },
    //根据地址设置更换地址省的默认值
    setProDefault: function (info) {
      console.log('进来了', info);
      var that = this;
      var province_s = that.data.province_s;
      for (var i in province_s) {
        if (province_s[i].id == info.province_info.id) {
          var a = 'value_sel[0]';
          that.setData({
            [a]: parseInt(i)
          })
          app.ajax({
            url: 'Common/Areas/areas',
            method: "POST",
            data: {
              parent_id: province_s[i].id
            },
            success: function (res) {
              if (res.data.code == 1000) {
                // console.log(res.data.data);          
                that.setData({
                  city_s: res.data.data
                });
                that.setCityDefault(info);
              } else {
                app.showToast(res.data.message);
              }
            }
          })
        }
      }

    },
    //根据地址设置更换地址市的默认值
    setCityDefault: function (info) {
      var that = this;
      var city_s = that.data.city_s;
      for (var i in city_s) {
        if (city_s[i].id == info.city_info.id) {
          var a = 'value_sel[1]';
          that.setData({
            [a]: parseInt(i)
          })
          app.ajax({
            url: 'Common/Areas/areas',
            method: "POST",
            data: {
              parent_id: city_s[i].id
            },
            success: function (res) {
              if (res.data.code == 1000) {
                // console.log(res.data.data);          
                that.setData({
                  area_s: res.data.data
                });
                that.setAreaDefault(info);
              } else {
                app.showToast(res.data.message);
              }
            }
          })
          break;
        }
      }
    },
    //根据地址设置更换地址区的默认值
    setAreaDefault: function (info) {
      var that = this;
      var area_s = that.data.area_s;
      for (var i in area_s) {
        if (area_s[i].id == info.area_info.id) {
          var a = 'value_sel[2]';
          that.setData({
            [a]: parseInt(i)
          })
          break;
        }
      }
    },
    // 监听valueSet变化
    changeValueSet(){
      var that = this;
      var value_Set = that.data.valueSet;
      console.log("设置默认地址", value_Set);
      if (value_Set.length > 0) {
        that.getCityLocation(value_Set[0], value_Set[1]);
      }
    },
    //监听valueName变化
    changeValueName(){
      var that = this;
      var value_Name = that.data.valueName;
      console.log('value_Name', value_Name);
      if (value_Name.length > 0) {
        that.getCityCode(value_Name[0], value_Name[1], value_Name[2]);
      }
    },

  },

  
})
