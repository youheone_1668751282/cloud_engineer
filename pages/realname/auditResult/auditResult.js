// pages/shopping/inAudit/inAudit.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rotate: 0,
    result: 1,
    message: '',
    user_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      result: options.result,
      user_id:options.user_id
    });
    setInterval(function() {
      if (that.data.rotate == 0) {
        that.setData({
          rotate: 90
        })
      } else if (that.data.rotate == 90) {
        that.setData({
          rotate: 0
        })
      }
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  //查看认证信息
  seeInfo() {
    var that = this;
    wx.redirectTo({
      url: '../myCertification/myCertification?user_id=' + that.data.user_id,
    })
  },
  //回到首页
  gohome() {
    wx.switchTab({
      url: '/pages/Allhome/Allhome',
    })
  },
   //回到个人中心
  nopeFun() {
    wx.switchTab({
      url: '/pages/myCenter/home/home',
    })
  }
})