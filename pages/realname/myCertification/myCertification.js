// pages/myCertification/myCertification.js
var app = getApp();
var rz_bg = app.globalData._network_path + 'rz_bg.png';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rz_bg: rz_bg,//背景
    usermagInfo:{},
    authInfo:{},
    user_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      user_id:options.user_id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getUserInfo();
    that.getAuthInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

 //跳转到基本信息
 baseInfo(){
   wx.navigateTo({
     url: '/pages/realname/realName/realName?user_id='+this.data.user_id,
   })
 },
  //跳转到身份证信息
  uploadDocuments() {
    wx.navigateTo({
      url: '/pages/realname/uploadDocuments/uploadDocuments?user_id=' + this.data.user_id,
    })
  },
  //获取用户信息
  getUserInfo() {
    var that = this;
    app.ajax({
      url: "Engineer/User/getUserInfo",
      data: { user_id: that.data.user_id},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            usermagInfo: res.data.data
          })
        } 
      }
    })
  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "Engineer/Realname/getAuthInfo",
      data: { user_id: that.data.user_id},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            authInfo: res.data.data,
          })
        }
      }
    })
  }
})