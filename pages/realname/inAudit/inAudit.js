// pages/shopping/inAudit/inAudit.js
var app = getApp();
var util = require("../../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subData: {},
    code: '',
    id_card: '',
    name: '',
    session_id: '',
    videoSrc: '',
    rotate: 0,
    isSuccess: 1,
    message: '',
    user_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      code: options.code,
      id_card: options.id_card,
      name: options.name,
      session_id: options.session_id,
      videoSrc: options.videoSrc,
      subData: wx.getStorageSync('subAuthData'),
    });
    if (util.isExitsVariable(options.user_id)) {
      that.setData({
        user_id: options.user_id
      })
    }
    that.upVideo();
    setInterval(function() {
      if (that.data.rotate == 0) {
        that.setData({
          rotate: 90
        })
      } else if (that.data.rotate == 90) {
        that.setData({
          rotate: 0
        })
      }
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },


  //上传视频
  upVideo() {
    var that = this;
    var token = wx.getStorageSync('token');
    var videoSrc = that.data.videoSrc;
    console.log(that.data.user_id,"u")
    wx.uploadFile({
      url: app.globalData._url + 'Engineer/Realname/step3',
      filePath: videoSrc,
      name: 'file',
      header: { token: token, openid: wx.getStorageSync('openid')},
      formData: {
        code: that.data.code,
        id_card: that.data.id_card,
        name: that.data.name,
        session_id: that.data.session_id,
        user_id:that.data.user_id,
      },
      success: function(res) {
        var data = JSON.parse(res.data);
        if (data.code == 1000) {
          that.setData({
            isSuccess: 2
          })
          wx.removeStorage({
            key: 'subAuthData',
            success: function (re) {
              //异步删除缓存
            }
          })
        } else if (data.code == -1110){
          that.setData({
            isSuccess: 4,
            message: data.message
          })
        }else{
          that.setData({
            isSuccess: 3,
            message: data.message
          })
        }
      },
      fail: function(err) {
        that.setData({
          isSuccess: 3

        })
      }
    })
  },
  // 重新认证
  again() {
    var that = this;
    wx.redirectTo({
      url: '../faceRecognition/faceRecognition?user_id='+that.data.user_id,
    })
  },

  // 暂不认证
  nope() {
    wx.switchTab({
      url: '../../order/myorder/myorder',
    })
  },
  //查看认证信息
  seeInfo(){
    var that = this;
    wx.redirectTo({
      url: '../myCertification/myCertification?user_id=' + that.data.user_id,
    })
  },
  //回到首页
  gohome(){
    wx.switchTab({
      url: '/pages/Allhome/Allhome',
    })
  }
})