// pages/wxParseDetail/wxParseDetail.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',//详情
    id: '',//
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var title = options.title || '';
    var id = options.id || '';
    that.setData({
      title: title,
      id: id
    })
    if (title != '') {
      wx.setNavigationBarTitle({
        title: title,
      })
    }
    that.getinfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 获取详情
  getinfo() {
    var that = this;
    app.ajax({
      url: 'Common/Version/getVersionDetail',
      data: {
        version_id: that.data.id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data,
          })
          var article = res.data.data.content;
          WxParse.wxParse('article', 'html', article, that, 5);

        }
      }
    })
  },
  // 返回
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  }
})