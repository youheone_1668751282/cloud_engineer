// pages/myCenter/ordersStatistics/ordersStatistics.js
const app = getApp();
var rate = 0;
var canvasWidth = 0;
var canvasHeight = 0;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token: wx.getStorageSync('token'),
    imgUrls: [
      {
        valu:'',
        types:'维修率',
        backClass: 'waiBackground1'
    },
      {
        valu: '', 
        types: '好评率',
        backClass: 'waiBackground2'
      },
    ],
    indicatorDots: true,
    autoplay: false,
    interval: 5000,
    duration: 1000,
    waytype: 1,//类型 1是七天 2是一月
    columnCanvasData: {
      canvasId: 'columnCanvas',
    },
    orderCountData:'',//数据统计
    completeRate: '',//维修率
    praiseRate: '',//好评率
    showMove: false,//滑动提示
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var systemInfo = app.systemInfo;
    rate = systemInfo.screenWidth / 750;
    var updateData = {};
    canvasWidth = systemInfo.screenWidth - rate * 64;
    canvasHeight = rate * 306 + rate * 44 + rate * 34 + rate * 22;

    var culumnYMax = 140;
    var culumnYMin = 0;
    updateData['columnCanvasData.canvasWidth'] = canvasWidth;
    updateData['columnCanvasData.axisPadd'] = { left: rate * 5, top: rate * 13, right: rate * 5 };
    updateData['columnCanvasData.axisMargin'] = { bottom: rate * 34, left: rate * 26 };
    updateData['columnCanvasData.yAxis.fontSize'] = rate * 22;
    updateData['columnCanvasData.yAxis.fontColor'] = '#637280';
    updateData['columnCanvasData.yAxis.lineColor'] = '#c2e9fb';
    updateData['columnCanvasData.yAxis.lineWidth'] = rate * 3;
    updateData['columnCanvasData.yAxis.dataWidth'] = rate * 39;
    updateData['columnCanvasData.yAxis.isShow'] = true;
    updateData['columnCanvasData.yAxis.isDash'] = true;
    updateData['columnCanvasData.yAxis.minData'] = culumnYMin;
    updateData['columnCanvasData.yAxis.maxData'] = culumnYMax;
    updateData['columnCanvasData.yAxis.padd'] = rate * 306 / (culumnYMax - culumnYMin);

    updateData['columnCanvasData.xAxis.dataHeight'] = rate * 26;
    updateData['columnCanvasData.xAxis.fontSize'] = rate * 22;
    updateData['columnCanvasData.xAxis.fontColor'] = '#637280';
    updateData['columnCanvasData.xAxis.lineColor'] = '#95B8E9';
    updateData['columnCanvasData.xAxis.lineWidth'] = rate * 3;
    updateData['columnCanvasData.xAxis.padd'] = rate * 52;
    updateData['columnCanvasData.xAxis.dataWidth'] = rate * 39;
    updateData['columnCanvasData.xAxis.leftOffset'] = rate * 20;


    updateData['columnCanvasData.canvasHeight'] = canvasHeight;
    updateData['columnCanvasData.enableScroll'] = true;

    this.setData(updateData);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that =this;
    that.getCountList();
    that.getCountRatio();
    var updateData = {};
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 获取数据
  getCountList: function(){
    var that = this;
    app.ajax({
      url: 'Engineer/Order/repairOrderCount',
      method: 'POST',
      load: load,
      msg: '数据加载中...',
      data:{},
      header:{'token': that.data.token},
      success: function(res){
        if(res.data.code == 1000){
          that.setData({
            orderCountData: res.data.data,
          });
          load = false;
          that.drawChart();
        }

      }
    })
  },
  // 画图表
  drawChart: function(){
    var that = this;
    var updateData = {};    
    var columnYMax = 140;
    var columnYMin = 0;
    updateData['columnCanvasData.yAxis.minData'] = columnYMin;
    updateData['columnCanvasData.yAxis.maxData'] = columnYMax;  
    var seven_everyday_num = that.data.orderCountData.seven_everyday_num;
    var month_everyday_num = that.data.orderCountData.month_everyday_num;
    var Days = [];
    var Data = [];
    if (that.data.waytype == 1){
      for (var key in seven_everyday_num) {
        Days.push(key);
        Data.push(seven_everyday_num[key]);
      }
      updateData['columnCanvasData.xAxis.data'] = Days;  
      updateData['columnCanvasData.series'] = [{
        data: Data
      }];    
    }else{
      for (var key in month_everyday_num) {
        Days.push(key);
        Data.push(month_everyday_num[key]);
      }
      updateData['columnCanvasData.xAxis.data'] = Days; 
      updateData['columnCanvasData.series'] = [{
        data: Data
      }];
    }
    updateData['columnCanvasData.yAxis.data'] = [
      { x: 0, y: 0, title: '0' },
      { x: 0, y: 20, title: '20' },
      { x: 0, y: 40, title: '40' },
      { x: 0, y: 60, title: '60' },
      { x: 0, y: 80, title: '80' },
      { x: 0, y: 100, title: '100' },
      { x: 0, y: 120, title: '120' },
      { x: 0, y: 140, title: '140' }
    ];
    
    this.setData(updateData);
  },
  //点击查看类型
  chooseit(e){
    var that=this;
    that.setData({
      waytype:e.currentTarget.dataset.ways
    })
    if (e.currentTarget.dataset.ways == 2){
      that.setData({
        showMove: true
      })
    }else{
      that.setData({
        showMove: false
      })
    }
    that.drawChart();
  },
  // 获取维修率、好评率
  getCountRatio: function(){
    var that = this;
    app.ajax({
      url:'Engineer/Engineers/workCompleteAndPraise',
      method: 'POST',
      data:{},
      header:{ 'token': that.data.token},
      success: function(res){
        if(res.data.code == 1000){
          var a = "imgUrls[" + 0 +"].valu";
          var b = "imgUrls[" + 1 + "].valu";
          that.setData({
            [a]: res.data.data.workCompletePercentage+"%",
            [b]: res.data.data.workPraisePercentage+"%"
          });
        }

      }
    })
  },
  onTouchHandler(e) {
    if (null == this.column_chart) {
      this.column_chart = this.selectComponent("#column-chart");
    }
    this.column_chart.onTouchHandler(e);
  },
  onTouchMoveHandler(e) {    
    if (null == this.column_chart) {
      this.column_chart = this.selectComponent("#column-chart");
    }
    this.column_chart.onTouchMoveHandler(e);
  },
  onTouchEndHandler(e) {    
    if (null == this.column_chart) {
      this.ccolumn_chart = this.selectComponent("#column-chart");
    }
    this.column_chart.onTouchEndHandler(e);
  },

})