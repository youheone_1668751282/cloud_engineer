// pages/myCenter/forgetPassword/forgetPassword.js
var app = getApp();
var countdown = 120;
var reget = '获取验证码';
var timer;
var isTimer = false;//timer是否开始

var settime = function (that) {
  if (countdown == 0) {
    that.setData({
      is_show: true
    })
    countdown = 120;
    reget = '重新获取';
    isTimer = false;
    return;
  } else {
    that.setData({
      is_show: false,
      last_time: countdown
    })
    isTimer = true;
    countdown--;
    reget = '重新获取';
  }
  timer = setTimeout(function () {
    settime(that)
  }
    , 1000)
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    screenWidth: 0, //屏幕宽度
    phone: '',
    getOnecode: reget,//内容
    last_time: '',//剩余时间
    is_show: true,//是否显示倒计时
    psdType:'password',//默认是密码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var phone = wx.getStorageSync('phone');
    that.setData({
      phone: phone
    })
    // 获取屏幕宽度
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          screenWidth: res.screenWidth
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    isTimer = false;
    // 清除timer
    clearTimeout(timer);

  },
  //是否查看密码
  lookShow(){
      var that=this;
    console.log('password');
    if (that.data.psdType ==='password'){
      that.setData({
        psdType:'text'
      })
    }else{
      that.setData({
        psdType: 'password'
      })
    }
  },
//手机号码
  setPhone(e) {
    this.setData({
      phone: e.detail.value,
    });
  },
  //校验手机号
  checkNum: function (str) {
    var inputNum = str;
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test(inputNum)) {
      return false;
    } else {
      return true;
    }
  },
  /**
   * 发送验证码
   */
  sendCode() {
    var that = this;
    console.log('isTimer:' + isTimer);
    if (isTimer) {
      console.log('timer已经开始');
      return;
    }
    if (that.data.phone == '') {
      app.showToast('手机号码不能为空'); return false;
    }
    if (!that.checkNum(that.data.phone)) {
      app.showToast('手机号码格式不正确'); return false;
    }
    isTimer = true;
    app.ajax({
      url: 'Common/Common/sendSms',
      data: {
        tel: that.data.phone,
      },
      success: function (res) {
       that.setData({
         is_show: (!that.data.is_show)  //false
       })
        countdown = 120;
        reget = '获取验证码';
       settime(that); //刷新倒计时 
       that.setData({
         getOnecode: reget
       })
        app.showToast(res.data.message, "none", 2500, function () { });
      }

    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

 
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //确认修改
  sureUpdate: function (e) {
    var that = this;
    var formData = e.detail.value;
    if (app.checkForm(formData.tel,'','手机号码不能为空')
      || app.checkForm(formData.code,'','请输入验证码')
      || app.checkForm(formData.newPassword,'','请输入新密码')
      || app.checkForm(formData.confimPassword,'','请确认新密码')
      ){
        return false;
    }
    if (!that.checkNum(formData.tel)) {
      app.showToast('手机号码格式不正确'); return false;
    }
    if (formData.newPassword !== formData.confimPassword){
      app.showToast('两次输入的密码不一致!'); return false;
    }
    app.ajax({
      url: 'Engineer/Engineers/forgetPassword',
      method: 'POST',
      data:formData,
      success:function(res){
        if(res.data.code == 1000){
          app.showToast("修改成功!2秒后跳转", "none", 1000, function () { });
          setTimeout(() => {
            wx.navigateBack({
              delta: 1
            })
          }, 1000);
        }else{
          app.showToast(res.data.message, "none", 1000, function () { });

        }
      }
    })

  },
})