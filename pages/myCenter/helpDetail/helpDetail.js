// pages/user/helpDetail/helpDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type_s: 0, //默认值为1
    helpList: [{
      name: '下单后几日可以上门安装？',
      content: '下单后48小时内会有工作人员与您电话联系确认上门时间，请保持手机畅通。上门安装时间一般不会超过5个工作日。'
    }, {
        name: '可以开具发票吗？',
        content: '下单时可以选择是否开具发票。'
    }, {
        name: '合同具有效力吗？',
        content: '凡是您阅读并同意签约，都具有法律效益。(在法律允许的范围合同对双方都具有约束力。)'
    }, {
        name: '申请售后后多久上门维修？',
        content: '申请售后48小时内会有工作人员与您电话联系确认上门时间，请保持手机畅通。上门维修时间一般不会超过5个工作日。'
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.setData({
      type_s: options.type_s
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

})