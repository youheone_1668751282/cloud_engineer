// pages/myCenter/warehousing/warehousing.js
var util = require("../../../utils/util.js");
var app = getApp();
var moreup = false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    values: '',
    scanning: '../../../images/scanning.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    moreup = false;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //input 输入框
  inputfun(e) {
    var that = this;
    that.setData({
      values: e.detail.value
    })
  },
  // 扫码
  scanning: function () {
    var that = this;
    app.scanning(function (data) {
      that.setData({
        values: data
      })
    })
    // wx.scanCode({
    //   success: (res) => {

    //     console.log(res)
    //     var code = res.result.split(';')
    //     if (!util.isExitsVariable(code) || code.length == 0) {
    //       app.showToast('扫码失败');
    //       return false;
    //     }
    //     var val = code[2].split(':');
    //     if (!util.isExitsVariable(val) || val.length == 0) {
    //       app.showToast('扫码失败');
    //       return false;
    //     }
    //     that.setData({
    //       values: val[1]
    //     })

    //   },
    //   fail: (res) => {
    //     app.showToast('扫码失败，请重试！');
    //   }
    // })
  },
  //确认事件 设备信息
  affirmfun(e) {
    var that = this;
    if (that.data.values == '' || that.data.values == null) {
      app.showToast("请输入设备编号", "none", 2000, function () { });
      return;
    }
    if (moreup) {
      console.log('??', moreup)
      return false
    }
    moreup = true;
    var URL = 'Common/Test/importEquipment';
    var device_no = that.data.values;
    //app.saveFormId(e.detail.formId);
    app.ajax({
      url: URL,
      data: {
        device_no: device_no
      },
      success: function (res) {
        app.showToast(res.data.message, "none", 2000, function () { });
        if (res.data.code == 1000) {
          that.setData({
            values: ''
          })
        } else {
          moreup = false
        }

      }
    })

  }
})