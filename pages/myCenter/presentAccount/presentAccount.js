// pages/user/presentAccount/presentAccount.js
var app = getApp();
var PAGE = 1;
var ROW = 5;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bankList:[],
    hasMore: false,
    loading: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.getbankList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    PAGE = 1;
    this.getbankList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    PAGE++;
    that.setData({
      loading: true
    })
    that.getbankList();
  },  


  // 添加银行卡
  addAccount: function () {
    wx.navigateTo({
      url: '../addeditAccount/addeditAccount?type=2',
    })
  },

  // 编辑
  eidtcard: function (e) {
    var bank_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../addeditAccount/addeditAccount?type=1&&bank_id=' + bank_id,
    })
  },

  // 删除
  delcard: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var bank_id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '是否删除该账户',
      success: function (res) {
        if (res.confirm) {
          var bankList = that.data.bankList         
          app.ajax({
            url: "Engineer/Account/deleteBank",
            data: {
              bank_id: bank_id
            },
            success: function (res) {
              app.showToast(res.data.message)   
              bankList.splice(index, 1);
              that.setData({
                bankList: bankList
              });        
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 获取银行卡列表
  getbankList(){
    var that = this;
    app.ajax({
      url: 'Engineer/Account/bankList',
      data: {
        page: PAGE,
        row: ROW,
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (PAGE == 1) {
            that.setData({
              bankList: res.data.data.data
            })
          } else {
            that.setData({
              bankList: that.data.bankList.concat(res.data.data.data)
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.data.length < that.data.row) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          if (PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
      }
    })
  }

})