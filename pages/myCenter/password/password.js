// pages/myCenter/password/password.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.loginStatus(function () {
      
    }, function () {
      app.showModal("", "您还未登录，请先登录", function () {
        wx.navigateTo({
          url: '/pages/myCenter/login/login',
        })
      });
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  //修改密码
  editePassword(e) {
    var that = this;
    var formData = e.detail.value;
    if (app.checkForm(formData.OldPassword,'','请输入旧密码')
      || app.checkForm(formData.NewPaasowrd,'','请输入新密码')
      || app.checkForm(formData.confirmNewPaasowrd, '', '请确认新密码')){
      return false;
    }
    app.ajax({
      url: 'Engineer/Engineers/engineersEditPassword',
      data: formData,
      success: function(res){
        if(res.data.code==1000){
          app.showToast("密码修改成功，请重新登录","none",2000,function(){
            wx.setStorageSync('isLogout', 2);
            setTimeout(function () {
              wx.removeStorageSync('token');
              wx.navigateTo({
                url: '../../myCenter/login/login'
              })
            }, 2000);
          });
          
        }else{
          app.showToast(res.data.message, "none", 2000, function () { });          
        }
      }
    })

  },
})