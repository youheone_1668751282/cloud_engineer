// pages/myCenter/userMsg/userMsg.js
var app = getApp();
let user_img = '';//头像半路径
Page({
  /**
   * 页面的初始数据
   */
  data: {
    gender: 1,//1是男的  2是女的
    engineersInfo:'',
    token: wx.getStorageSync('token')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that =this;
    app.loginStatus(function(){
      that.getEngMsg();  
    },function(){
      app.showModal("", "您还未登录，请先登录", function () {
        wx.navigateTo({
          url: '/pages/myCenter/login/login',
        })
      });
    })
  },

 
  // 选择头像
  chooseimage: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ['从相册中选择', '拍照'],
      itemColor: "#003a70",
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {
            that.chooseWxImage('album')
          } else if (res.tapIndex == 1) {
            that.chooseWxImage('camera')
          }
        }
      }
    })
  },
  // 头像 上传路径
  chooseWxImage: function (type) {
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: [type],
      success: function (res) {
        user_img = res.tempFilePaths[0];
        wx.uploadFile({
          url: app.globalData._url + 'Common/Common/upload', // 头像上传接口地址
          filePath: user_img,
          name: 'file',
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1000) {
              var img = data.data.url; 
              user_img = img;
              that.editUserImg();
            } else {
              app.showToast("上传头像失败");
            }
          }
        })
      }
    })
  },
  // 修改头像 调用接口............
  editUserImg() {
    var that = this;
    var headimg = user_img;
    app.ajax({
      url: 'Engineer/Engineers/engineersEditInfo',
      load: true,
      msg: '头像正在上传~~',
      data: { avatar_img: headimg},
      header: {'token': that.data.token},
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast("上传头像成功");
          that.getEngMsg();             
        }

      }
    })
  },
  // 选择性别
  choosesex: function () {
    var that = this;
    wx.showActionSheet({
      itemList: ['男', '女'],
      itemColor: "#003a70",
      success: function (res) {
        if (!res.cancel) {
          if (res.tapIndex == 0) {
            that.setData({
              gender: 1
            })
          } else if (res.tapIndex == 1) {
            that.setData({
              gender: 2
            })
          }
          var gender = that.data.gender;
          app.ajax({
            url: 'Engineer/Engineers/engineersEditInfo',
            method: 'POST',
            data: { gender: gender},
            header:{ 'token': that.data.token},
            success: function(res){
              if(res.data.code == 1000){
                app.showToast("修改成功","none",2000,function(){});
                that.getEngMsg();
              }else{
                app.showToast(res.data.message);                
              }
            }
          })


        }
      }
    })
  },
  
  //修改名称
  changeName(e) {
    var name = e.currentTarget.dataset.name;
    wx.navigateTo({
      url: '../changeName/changeName?name='+name,
    })
  },
  //修改密码
  password() {
    wx.navigateTo({
      url: '../password/password',
    })
  },
  // 获取用户信息
  getEngMsg: function () {
    var that = this;
    app.ajax({
      url: "Engineer/Engineers/engineersInfo",
      data: {},
      header:{ 'token': that.data.token},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            engineersInfo: res.data.data,
            gender: res.data.data.gender
          })
          user_img = res.data.data.avatar_img;
        } else {
          that.setData({
            engineersInfo: ''
          })
        }
      }
    })
  },
  //打开授权设置
  authorization() {
    var that = this;
    wx.openSetting({
      success(res) {
        // if (res.authSetting['scope.userLocation'] == true) {
        //   that.position();
        // }
      }
    })
  },
  // 退出登录
  logout: function(){
    var that =this;
    wx.setStorageSync('isLogout', 1);
    wx.removeStorageSync('token');
    that.setData({
      engineersInfo: ""
    })
    app.showToast("退出成功");
    setTimeout(()=>{
      wx.navigateTo({
        url: '/pages/myCenter/login/login',
      })
    },2000)    
  }
  

})