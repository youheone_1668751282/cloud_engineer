/////// pages/myCenter/login/login.js
var app = getApp();
var util = require("../../../utils/util.js");  
var loginBack = app.globalData._network_path + 'loginBack.png';
let num = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logo: '',
    userInfo: '',//获取到的用户信息
    pwd: '',
    testInfo:'',
    loginBack: loginBack,//背景图片
    typeLogin: 1, //登录类型，1工程人员、2行政人员
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.type == 1 || options.type == 2){
        this.setData({
          typeLogin: options.type
        })
      }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.setStorageSync('is_login', false);
    that.getCount();
    that.getCompanyImg();  // 获取图片LOGO
  },

  // 获取logo
  getCompanyImg: function () {
    var that = this;
    var company_id = app.globalData.company_id
    // ajax请求
    app.ajax({
      url: 'Api/Company/getCompayConfig',
      url_type: 1,
      data: { company_id: company_id },
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            logo: res.data.data.img_config.image
          })
        }
      }
    })
  },

  // 获取测试账号
  getCount:function(){
    var that = this;
    app.ajax({
      url:'Common/Test/engineer_account',
      data:{},
      success:function(res){
        if(res.data.code==1000){
          that.setData({
            testInfo: res.data.data
          })
        }
      }
    })
  },
 
  // 输入手机号
  inputNum:function(e){
    var that =this;
    num = e.detail.value;
  },
  //输入密码
  inputPwd: function (e) {
    var that = this; 
    that.setData({
      pwd: e.detail.value
    })

  },

  //切换登录
  checkLogin(e) {
    var type = e.currentTarget.dataset.type;
    this.setData({
      typeLogin: type
    })
  },

  //登录
  login() {
    var that = this;
    if(that.data.typeLogin == 1) {
      if (num == '') {
        app.showToast('手机号不能为空！', 'none', 2000, function () { });
        return;
      }
      if (that.data.pwd == '') {
        app.showToast('密码不能为空！', 'none', 2000, function () { });
        return;
      }
      if (!that.checkNum(num)) {
        return;
      }
      if (!util.isExitsVariable(wx.getStorageSync('openid'))) {
        app.redirectLogin();
        return;
      }
      app.ajax({
        url: 'Engineer/Engineers/login',
        data: {
          engineers_phone: num,
          engineers_password: that.data.pwd,
          openid: wx.getStorageSync('openid')
        },
        success: function (res) {
          if (res.data.code == 1000) {
            wx.setStorageSync("account", num);
            wx.setStorageSync('token', res.data.data.token);
            wx.setStorageSync('loginType', that.data.typeLogin);
            app.showToast("登录成功", "none", 2000, function () { });
            wx.setStorageSync('login_time', new Date().getTime());
            //   app.webSocket();
            var isLogout = wx.getStorageSync('isLogout');
            var delta;
            if (isLogout == 1) {
              delta = 2;
            } else if (isLogout == 2) {
              delta = 3;
            } else {
              delta = 1;
            }
            setTimeout(() => {
              wx.switchTab({
                url: '../../takeOrders/takeOrders/takeOrders',
              })
            }, 2000);

          } else {
            app.showToast(res.data.message, "none", 2000, function () { });
            return false;
          }
        }
      });
    } else {
      if (num == '') {
        app.showToast('账号不能为空！', 'none', 2000, function () { });
        return;
      }
      if (that.data.pwd == '') {
        app.showToast('密码不能为空！', 'none', 2000, function () { });
        return;
      }
      if (!util.isExitsVariable(wx.getStorageSync('openid'))) {
        app.redirectLogin();
        return;
      }
      app.ajax({
        url: 'api/Login/doLogin',
        url_type: 2,
        data: {
          account: num,
          password: that.data.pwd,
          openid: wx.getStorageSync('openid')
        },
        success: function (res) {
          if (res.data.code == 1000) {
            wx.setStorageSync("account", num);
            wx.setStorageSync("client", res.data.data.client);//区分是总后台、行政中心还是运营中心;
            wx.setStorageSync('token', res.data.data.token);
            wx.setStorageSync('loginType', that.data.typeLogin);
            app.showToast("登录成功", "none", 2000, function () { });
            wx.setStorageSync('login_time', new Date().getTime());
            //   app.webSocket();
            var isLogout = wx.getStorageSync('isLogout');
            var delta;
            if (isLogout == 1) {
              delta = 2;
            } else if (isLogout == 2) {
              delta = 3;
            } else {
              delta = 1;
            }
            // setTimeout(() => {
              wx.reLaunch({
                url: '/pages/subpackage/pages/crm/index/index',
              })
            // }, 2000);

          } else {
            app.showToast(res.data.msg, "none", 2000, function () { });
            return false;
          }
        }
      });
    }
  },
  //校验手机号
  checkNum: function(str){
    var inputNum = str;
    var myreg = /^[1][3-9][\d]{9}$/;
    if (!myreg.test(inputNum)) {
      app.showToast('无效的手机号码', "none", 2000, function () { });
      return false;
    }else{
      return true;
    }
  },
  //忘记密码
  forgetPassword(){
    wx,wx.navigateTo({
      url: '../forgetPassword/forgetPassword',
    })
  },
  //复制账号
  copyacount: function(e){
    var account = e.currentTarget.dataset.account;
    console.log(account);
    wx.setClipboardData({
      data: account,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            console.log(res.data) // data
          }
        })
      }
    })
  },
  //复制密码
  copypwd: function (e) {
    var pwd = e.currentTarget.dataset.pwd;
    wx.setClipboardData({
      data: pwd,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            console.log(res.data) // data
          }
        })
      }
    })
  },
})