// pages/myCenter/home/home.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    engineersInfo: '',
    login_status:false
  },

  

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    app.loginStatus(function(){
      that.getEngMsg(); 
      that.setData({
        login_status:true
      }) 
    },function(){
      that.setData({
        login_status: false,
        engineersInfo:''

      }) 
    })
    
    
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 跳转我的账户
  goAccount(){
    wx.navigateTo({
      url: '../accounputForward/accounputForward',
    })
  },
  // 跳转账户记录
  goAccountRecords(){
    wx.navigateTo({
      url: '../accountRecords/accountRecords',
    })
  },
  //跳转服务站
  serviceStation(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../serviceStation/serviceStation?id='+id,
    })
  },
  //跳转用户信息
  userMsg(){     
    wx.navigateTo({
      url: '../userMsg/userMsg',
    })
  },
  //跳转登录
  login(){
    wx.navigateTo({
      url: '../login/login',
    })
  },
  //跳转订单统计
  ordersStatistics(){
    
    wx.navigateTo({
      url: '../ordersStatistics/ordersStatistics',
    })
  },
  // 跳转我的订单
  myorder(){
    wx.navigateTo({
      url: '../../order/myAllorder/myAllorder',
    })
  },
  //财务管理
  financeManagement(){
   
    wx.navigateTo({
      url: '../financeManagement/financeManagement',
    })
  },
  //跳转帮助中心
  helpCenter(){
      wx.navigateTo({
        url: '../helpCenter/helpCenter',
      })
  },
  
  // 跳转收付款记录
  payRecord: function(){
    wx.navigateTo({
      url: '../../order/payRecord/payRecord?type=1',
    })
  },
  //获取用户头像信息
  bindgetuserinfofun: function (e) {
    var that = this;
    if (e.detail.errMsg == 'getUserInfo:fail:cancel to confirm login' ||
      e.detail.errMsg == 'getUserInfo:fail auth deny' || e.detail.rawData == 'undefined') {
      app.showToast("取消将无法修改用户信息", "none", 2000, function () { });
    } else {
      // that.setData({
      //   userInfo: e.detail.userInfo
      // })
      wx.setStorageSync('user_Info', e.detail.userInfo);//缓存头像
      //wx.setStorageSync('nickName', e.detail.userInfo.nickName);//缓存名称
    }
  },
  //调试设备
  deviceTest(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/test/test',
    })
  },
  // 获取用户信息
  getEngMsg: function(){
    var that = this;
    app.ajax({
      url: "Engineer/Engineers/engineersInfo",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            engineersInfo: res.data.data
          })
        } else {
          that.setData({
            engineersInfo:''            
          })
        }
      }
    })
  },
  // 设备入库
  warehousing: function(){
    wx.navigateTo({
      url: '../warehousing/warehousing',
    })
  },
  //跳转设备替换
  navDeviceReplace(){
    wx.navigateTo({
      url: '../deviceReplace/deviceReplace?work_order_id=-1',
    })
  },
  //跳转绑定主板
  navBinding(){
    wx.navigateTo({
      url: '../binding/binding',
    })
  },
  //跳转解绑主板
  navUnbind() {
    wx.navigateTo({
      url: '../unbind/unbind',
    })
  },
  //跳转版本更新
  versionUpdate() {
    wx.navigateTo({
      url: '../versionUpdate/versionUpdate',
    })
  },
})
