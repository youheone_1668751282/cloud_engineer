// pages/user/accounputForward/accounputForward.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    workInfo: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.workerInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getRule();
    this.workerInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  // 提现
  withdrawal(e) {
    var that = this;
    // var is_have_bank = that.data.workInfo.is_have_bank;
    // var workers_id = e.currentTarget.dataset.id;
    // console.log('is_have_bank', is_have_bank)
    wx.navigateTo({
      url: '../putForwardPrice/putForwardPrice'
    })
    // if (is_have_bank){
    //   wx.navigateTo({
    //     url: '../putForwardPrice/putForwardPrice'
    //   })
    // }else{
    //   wx.showModal({
    //     content: '请先绑定银行卡',
    //     showCancel: false,
    //     success(res) {
    //       if (res.confirm) {
    //         wx.navigateTo({
    //           url: '../addeditAccount/addeditAccount?type=2',
    //         })
    //       }
    //     }
    //   })
    // }
    
  },

  // 获取经销商信息
  workerInfo() {
    var that = this;
    app.ajax({
      url: "Engineer/Account/engineers",
      data: {},
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            workInfo: res.data.data,
          })
        }
      }
    })
  },

  // 提现记录
  presentRecord(){
    var that = this;
    // var workers_id = that.data.workInfo.workers_id
    wx.navigateTo({
      url: '../presentRecord/presentRecord',
    })
  },
  //业绩记录
  achievementRecord() {
    var that = this;
    // var workers_id = that.data.workInfo.workers_id
    wx.navigateTo({
      url: '../achievementRecord/achievementRecord',
    })
  },
  
  // 提现账户设置
  presentAccount(){
    var that = this;
    // var workers_id = that.data.workInfo.workers_id
    wx.navigateTo({
      url: '../presentAccount/presentAccount',
    })
  },
  // 获取提现说明
  getRule() {
    var that = this;
    app.ajax({
      url: 'Common/Article/getArticle',
      data: {
        cate_sn: 'AVwsyFwB'
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var article = res.data.data[0].content;
          WxParse.wxParse('article', 'html', article, that, 5);
        }
      }
    })
  },
})