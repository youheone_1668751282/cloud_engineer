// pages/shopping/Econtract/Econtract.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    look_icon: '../../../images/look_icon.png',
    agree_img:'../../../images/agree.png',
    contract_id:'',//合同id
    contractInfo:{},
    original_parts_html:'',
    equipments_info_html:'',
    isshow:false,
    load_show:false,
    is_auth: '',
    sign_way:'',
    user_id:'',
    h:'',
    work_order_id:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.showLoading({
      title: '加载中...',
    })
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          h: res.windowHeight - 50
        })
      },
    })
    that.setData({
      contract_id: options.contract_id,
      user_id:options.user_id,
      work_order_id: options.work_order_id,
    })
   
    that.getUserInfo();
  },

  //
   onShow:function(){
    var that = this;
    that.getContractInfo();
  },

  //获取合同签署方式
  getSystemConfig: function () {
    var that = this;
    app.ajax({
      url: 'Common/Common/getSystemConfig',
      data: {
        'key': 'sign_way',
        'user_id':that.data.contractInfo.rental_user_info.user_id
      },
      success: function (res) {
        var sign_way = 1;
        if (res.data.data == 2) {
          sign_way = 2;
        }
        that.setData({
          sign_way: sign_way
        })
      }
    })
  },

//  签订合同
  bindAgree:function(){
    var that = this;
    var is_auth = that.data.is_auth;
    if (is_auth==1){
      app.ajax({
        url: "Engineer/Realname/getAuthInfo",
        data: { user_id: that.data.user_id },
        success: function (res) {
          if (res.data.code == 1000 && res.data.data.status == -2) {
            wx.navigateTo({
              url: '../../realname/uploadDocuments/uploadDocuments?user_id=' + that.data.user_id,
            })
          } else if (res.data.code == 1000 && res.data.data.status == -3) {
            wx.navigateTo({
              url: '../../realname/faceRecognition/faceRecognition?user_id=' + that.data.user_id,
            })
          } else if (res.data.code == 1000 && (res.data.data.status == 2 || res.data.data.status == 1)) {
            wx.navigateTo({
              url: '../../realname/myCertification/myCertification?user_id=' + that.data.user_id,
            })
          } else {
            wx.navigateTo({
              url: '../../realname/realName/realName?user_id=' + that.data.user_id,
            })
          }
        }


      })
    }else{
      if(that.data.sign_way==2){//电子签名
      wx.navigateTo({
        url: '../sign/sign?contract_id=' + that.data.contract_id + "&user_id=" + that.data.user_id + "&realname=" + that.data.contractInfo.rental_user_info.realname + "&work_order_id=" + that.data.work_order_id,
      })
      }else{//确认同意
        app.ajax({
          url: 'Engineer/Contract/sign',
          method: "POST",
          data: {
            contract_id: that.data.contract_id,
            contact_type:1,
            sign_client:2,
            user_id:that.data.user_id,
            work_order_id: that.data.work_order_id,
          },
          success: function (res) {
            //console.log(res)
            if (res.data.code == 1000) {
              that.setData({
                isshow: true
              })
              app.showToast(res.data.message);
              // wx.navigateBack({
              //   delta:1
              // })
            } else {
              app.showToast(res.data.message);
            }
          }
        })
    }
    }
    
  },

  //获取合同信息
  getContractInfo() {
    var that = this;
    app.ajax({
      url: 'Engineer/Contract/getContractInfo',
      method: "POST",
      data: {
        contract_id: that.data.contract_id
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 1000) {
          //console.log(res.data.data);
          var contractInfo = res.data.data;
          var original_parts_html="";
          if (contractInfo.original_parts.length > 0) {
            for (var i = 0; i < contractInfo.original_parts.length;i++){
              original_parts_html += contractInfo.original_parts[i].parts_name + ';';
            }
          }
          var equipments_info_html = '';
          // if (contractInfo.equipments_info.list.length>0){
          //   equipments_info_html += contractInfo.equipments_info.list[0].equipments_name + '(';
          //   for (var i = 0; i < contractInfo.equipments_info.list.length; i++) {
          //     equipments_info_html += contractInfo.equipments_info.list[i].equipments_number+';';
          //   }
          //   equipments_info_html += ')';
            
          // }
          if (contractInfo.equipments_info.list.length > 0) {
            equipments_info_html = contractInfo.equipments_info.list[0].equipments_name;
          }
          var status = contractInfo.contract_info.status;
          var isshow = false;
          if (status==4){
            isshow = true;
          }
          that.setData({
            contractInfo: contractInfo,
            original_parts_html: original_parts_html,
            equipments_info_html: equipments_info_html,
            isshow: isshow
          })
          that.getSystemConfig();
        //  that.getCompany(contractInfo.contract_info.company_id);  // 获取入驻商信息
        }else {
          if (res.data.code == -1101) {
            app.showModal("",res.data.message,function(){
              var timer = setTimeout(function () {
                var user_id = that.data.user_id;
                app.ajax({
                  url: "Engineer/Realname/getAuthInfo",
                  data: { user_id: user_id },
                  success: function (res) {
                    if (res.data.code == 1000 && res.data.data.status == -2) {
                      wx.navigateTo({
                        url: '../../realname/uploadDocuments/uploadDocuments?user_id=' + user_id,
                      })
                    } else if (res.data.code == 1000 && res.data.data.status == -3) {
                      wx.navigateTo({
                        url: '../../realname/faceRecognition/faceRecognition?user_id=' + user_id,
                      })
                    } else if (res.data.code == 1000 && (res.data.data.status == 2 || res.data.data.status == 1)) {
                      wx.navigateTo({
                        url: '../../realname/myCertification/myCertification?user_id='+user_id,
                      })
                    }else {
                      wx.navigateTo({
                        url: '../../realname/realName/realName?user_id=' + user_id,
                      })
                    }
                  }
                 

                })

              }, 1000);
            });
          }
        }
        wx.hideLoading();
        that.setData({
          load_show: true
        })
      }
    })
  },

  // 获取入驻商信息
  getCompany: function (company_id) {
    var that = this;
    app.ajax({
      url: 'api/Company/getCompany',
      method: "POST",
      url_type: 1,
      data: {
        company_id: company_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var contractInfo = that.data.contractInfo;
          contractInfo.company_info = res.data.data;
          that.setData({
            contractInfo: contractInfo
          })
          console.log('入驻商结果', res)
        }

      }
    })
  },

  //获取验证信息
  getAuthInfo: function () {
    var that = this;
    app.ajax({
      url: 'Engineer/Realname/getAuthInfo',
      method: "POST",
      data: { user_id: that.data.user_id },
      success: function (res) {
        console.log(res)
        if (res.data.code == 1000) {
          var imgArr = [];
          if (res.data.data.auth_type == 1) {
            imgArr.push(res.data.data.front_idcard_pic_all);
            imgArr.push(res.data.data.back_idcard_pic_all);
          } else {
            imgArr.push(res.data.data.business_license_pic_all);
          }
          wx.previewImage({
            current: imgArr[0], // 当前显示图片的http链接
            urls: imgArr // 需要预览的图片http链接列表
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },

  //获取用户信息
  getUserInfo: function () {
    var that = this;
    app.ajax({
      url: 'Engineer/User/getUserInfo',
      method: "POST",
      data: {user_id:that.data.user_id},
      success: function (res) {
        //console.log(res)
        if (res.data.code == 1000) {
          var is_auth = res.data.data.is_auth;
          //console.log(is_auth);
          that.setData({
            is_auth: is_auth
          })
        } else {
          // app.showToast(res.data.message);
        }
      }
    })
  },
  // 返回上一页
  goBack(){
    wx.navigateBack({
      delta:1
    })
  },
  
})