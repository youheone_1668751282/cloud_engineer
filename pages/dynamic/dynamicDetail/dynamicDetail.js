// pages/dynamic/dynamicDetail/dynamicDetail.js
var app = getApp();
var WxParse = require('../../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    data: '',
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var that = this;
    that.setData({
      id: id
    });
    that.getDetail(id);
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 获取通知消息动态详情
  getDetail: function(id){
    var that = this;
    app.ajax({
      url: 'Common/Dynamic/dynamicDetail',
      data:{
        type: 1,
        dynamic_id: id
      },
      success: function(res){
        var data = res.data.data;
        var content = data.content;
        WxParse.wxParse('article', 'html', content, that, 5);
        that.setData({
          data: res.data.data
        })
      }
    })
  }

})