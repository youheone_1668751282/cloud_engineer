// pages/dynamic/myDynamic/dynamic.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_request: 1,  // 开关操作
    dynamic: [],   // 通知消息
    Order: [],    // 订单动态
    isActive: 1,  // 动态类型值
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    app.loginStatus(function () {
    }, function () {
    })
    var isActive = parseInt(wx.getStorageSync('dynActive')) || 1;
    console.log(isActive);
    this.setData({
      isActive: isActive,
    })
    load = true;
    console.log(that.data);
    _PAGE = 1;
    if(isActive == 1){
      that.getDynamicData(); // 通知消息列表      
    }else if( isActive == 2){
      that.getOrderData(); // 订单动态      
    }
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var isActive = that.data.isActive; // 类型值
    var hasMore = that.data.hasMore; // 开关值
    if(!hasMore){
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    // 通知消息
    if (isActive == 1) {
      that.getDynamicData();  // 通知消息
    }
    if (isActive == 2) {
      that.getOrderData(); // 订单动态
    }
    
  },

  // 获取通知消息数据
  getDynamicData: function () {
    var that = this;
    app.ajax({
      url: 'Common/Dynamic/dynamicList',
      method: "POST",
      load: load,
      msg: '动态加载中...',
      data: { 'type': 1, page: _PAGE , pageSize: _PAGESIZE},
      success: function (res) {
        if (res.data.code == 1000) {
          if(_PAGE == 1){
            that.setData({
              dynamic: res.data.data,
              empty: false
            })
          }else{
            that.setData({
              dynamic: that.data.dynamic.concat(res.data.data),
              empty: false
            })
          } 
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        }else{
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    })      
      
  },

  // 通知消息详情点击
  clickDetail: function (e) {
    var that = this;
    var dynamic_id = e.currentTarget.dataset.id;  // 通知消息ID
    wx.navigateTo({ 
      url: "../../dynamic/dynamicDetail/dynamicDetail?id=" + dynamic_id
    })
  },

  // 获取订单动态数据
  getOrderData: function () {
    var that = this;
    
    app.ajax({
      url: 'Engineer/Order/repairOrderDynamic',
      load: load,
      msg: '动态加载中...',
      data: { page: _PAGE, pageSize: _PAGESIZE },
      success: function (res) {
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              Order: res.data.data,
              empty: false
            })
          } else {
            that.setData({
              Order: that.data.Order.concat(res.data.data),
              empty: false
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    })
  },

  // 订单动态详情点击
  orderclickDetail: function (e) {
    var that = this;
    var work_order_id = e.currentTarget.dataset.id; // 订单动态ID
    wx.navigateTo({
      url: '../../order/orderDetail/orderDetail?id=' + work_order_id,
    })
  },

  // 动态切换
  changeDynamic: function (e) {
    var that = this;
    var isActive = e.currentTarget.dataset.type;
    wx.setStorageSync('dynActive', isActive);
    _PAGE = 1; // 重置数据
    that.setData({
      empty: true,
      hasMore: false,
      loading: false,
      loadShow: false
    })
    load = true;
    // 订单动态
    if (isActive == 1) {
      that.getDynamicData();  // 通知动态
    }
    // 订单动态
    if (isActive == 2) {
      app.loginStatus(function () {
        that.getOrderData();  // 订单动态
      }, function () {
        app.showModal("", "您还未登录，请先登录", function () {
          wx.navigateTo({
            url: '/pages/myCenter/login/login',
          })
        });
      })
      
    }
    this.setData({
      isActive: isActive
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    var that =this;
    that.setData({
      loadShow: false
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },
  //图片加载错误
  dynamic_error(e) {
    var that = this, dynamic = that.data.dynamic, inds = e.currentTarget.dataset.index;
    dynamic[0].dynamicData[0].image = '../../../images/none.png'
    that.setData({
      dynamic: dynamic
    })
  },
  //图片加载错误
  images_error(e) {
    var that = this, dynamic = that.data.dynamic, inds = e.currentTarget.dataset.index, nowIndex = e.currentTarget.dataset.nowindex;
    dynamic[inds].dynamicData[nowIndex].image = '../../../images/none.png'
    that.setData({
      dynamic: dynamic
    })
  },
  //图片加载错误
  Order_error(e) {
    var that = this, Order = that.data.Order, nowIndex = e.currentTarget.dataset.nowindex;
    Order[nowIndex].main_pic = '../../../images/none.png'
    that.setData({
      Order: Order
    })
  }
})