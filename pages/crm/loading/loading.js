// pages/crm/loading/loading.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /*** 生命周期函数--监听页面加载*/
  onLoad: function (options) {
    console.log('执行页面代码')
    setTimeout(function(){
      wx.reLaunch({
        url: '/pages/subpackage/pages/crm/index/index',//CRM首页
        //url: '/pages/subpackage/pages/crm/customerManag/customerManag'//客户管理
        //url: '/pages/subpackage/pages/crm/exchange/exchange' //交换
        //url: '/pages/subpackage/pages/crm/certification/certification'//实名认证
        //url: '/pages/subpackage/pages/crm/reportList/reportList'//报备列表
        //url: '/pages/subpackage/pages/crm/reportEdit/reportEdit'//报备编辑
        //url: '/pages/subpackage/pages/crm/examineEdit/examineEdit'//审核编辑
        //url: '/pages/subpackage/pages/crm/customerEdit/customerEdit'//客户编辑
        //url: '/pages/subpackage/pages/crm/financeManag/financeManag'//财务管理
        //url: '/pages/subpackage/pages/crm/workOrderManag/workOrderManag'//工单管理
        //url: '/pages/subpackage/pages/crm/dispatch/dispatch'//派单
        //url: '/pages/subpackage/pages/crm/backVisit/backVisit'//回访
        //url: '/pages/subpackage/pages/crm/addworkOrder/addworkOrder'//新增工单
        //url: '/pages/subpackage/pages/crm/defineSetMeal/defineSetMeal'//编辑工单的(自定义)
        //url: '/pages/subpackage/pages/crm/deviceManage/deviceManage'//设备管理
        //url: '/pages/subpackage/pages/crm/workOrderInfo/workOrderInfo'//工单查看
      })
    },1000)

  },
  onShow: function () {

  }
})
